## Build Notes for Clank-LZ

*this page occasionally loads slowly*

### The CAD 

In Fusion 360:

![final](images/2020-10-01_clank-final.png)

And in Rhino:

![rhino](images/2021-03-17_clank-lz-rhino.png)

Clank was developed in Fusion 360. The most recent .f3z (Fusion 360 Archive) and .step files are available in [the CAD folder](../cad) in this repo. These files have *enough* hardware included that one should be able to figure out which bolts / etc go where, but are not complete (i.e. in a group of four bolts, three may be missing). 

[the latest fusion 360 file](../cad/2020-09-29_clank-lz.f3z)  
[the latest step file](../cad/2020-09-29_clank-lz.step)  
[the latest rhino file (thanks Zain!)](../cad/2020-09-29_clank-lz.3dm)  

### The BOM 

A Bill of Materials for the lz machine is available in [this spreadsheet](https://docs.google.com/spreadsheets/d/1HzgdPIvii0NI3s8gkg9xFz3LI2AqHLVXkGTv0clRpXw/edit?usp=sharing). Most parts can be sourced from McMaster, DigiKey or Amazon (in the US). Other vendors include SDP-SI and Stepper-Online. 

Hardware totals to around $400 USD for the machine.  

Controllers used here cost about $250 USD.  

### The Controller

Our versions of Clank use experimental controllers that aren't available publicly, but the machine could easily be configured for use with any off-the-shelf GCode interpreter.  

[Clank-LZ Controller Repository](https://gitlab.cba.mit.edu/jakeread/clank-lz-controller)

### Build Notes

Bunch of videos, more or less in order:  

[Kit Contents Video](https://vimeo.com/465181796/d9b1418a1d)  
[Frame Build Video](https://vimeo.com/465177151/cdfbe2b179)  
[Y Left Build Video](https://vimeo.com/465177393/a3503891a2)  
[Y Right Build Video](https://vimeo.com/465178123/476ce2ce12)  
[X Carriage Build Video](https://vimeo.com/465178477/572ba36eab)  
[Z and Spindle Build Video](https://vimeo.com/465179336/25a84acffd)  
[Final Assembly Video](https://vimeo.com/465180122/a302f6a887)  
[Wiring Video](https://vimeo.com/465181178/928f6d7715)  

If vimeo is down or slow, you can also download these videos [here in the repo](video/)

### Spindle Press Tools

Pressing the spindle shaft into the shaft bearings & spindle body is best done with some tooling, I have two for this:

[upper](tools/clank-lz_spindle-press-tool-upper.stl)  
[lower](tools/clank-lz_spindle-press-tool-lower.stl)   

Print these **at 100% infill** and use an arbor press to press the shaft into the bearings, and then the shaft / bearing into the spindle housing. 

### Rob's Build Notes

[here](https://roberthart56.github.io/projects/Clank2020/) 

#### Final Checklist 

Before you turn your machine on, please do these checks:
- The spindle and spindle motor are well connected: try holding the spindle motor stationary while turning the tool holder (the bottom part of the spindle)
- Earth, Neutral and Line connections between the power supply and the wall voltage are correct (there are E/N/L labels on the fuse module, double check)
- PSU is set to 115v, not 220v. There is a slide switch on the side of the PSU.
- Your 10-pin IDC cable is oriented correctly: see the wiring video to check.
- The small Black & Green cable from the 20A ESC to the ESC Breakout Board is plugged in with the Black side on the left. 
- The motor boards are appropriately mounted: YL on the Y Left, X on the X, etc.

To power up:
- always plug the USB logic in first, before switching the PSU on.
- when you plug the USB in to your computer, there should be at least one LED turned on at each of the 6 boards. if this isnt' the case, something is wrong 
- if they are all on, you should be OK to boot [the controller](https://gitlab.cba.mit.edu/jakeread/clank-lz-controller) and mill some things 

### Print Settings

Most parts not-on-the-bom for these machines are 3D Printed: exceptions are the Motor Plates (which can be printed, if you'd like) which we mill in Phenolic (to prevent softening against warm motors), and the Bed which is milled from a sheet of HDPE. 

Printing parts should work with most settings: definitely larger infills and higher perimeter counts than standard settings will make better machines. These are what I use:  

In either Prusa Slic3r, or Slic3r itself:

- 0.4mm nozzle 
- 0.2mm layer height 
- 'expert settings' (top right) / layers & perimiters / check 'external perimeters first'
- layers & perimeters / perimeters: 5
- layers & perimeters / horizontal shells: top: 6, bottom: 6 
- infill, either:
    - 100%, rectilinear 
    - 40-60%, grid or line
    - both: combine infill every: 2 layers 

### Total Print Times & Weights 

These are rough notes from an early prototype, not exact, but very close.  

| Plate | Contents | Time | Filament (g) | Infill | Notes |
| --- | --- | --- | --- | --- | --- |
| 01 | Spindle Motor Interface | ~ | ~ | ~ | Revision TBD |
| 02 | Spindle Core | 14:59 | 228 | 50 Grid |
| 03 | XZU Lower Plate | 8:40 | 93 | 50 Grid |
| 04 | XZU Upper Plate | 9:26 | 124 | 50 Grid | Solid Modifier at Motor Mount |
| 05 | YL YR Plates | 15:13 | 179 | 50 Grid | Solid Modifier at YR Top Bridge |
| 06 | XZU Face, YL Side, Y Belt Ends, LMU Clamp | 11:13 | 122 | 50 Grid |
| 07 | Corners Rear | 16:57 | 288 | 40, Line | 
| 08 | Corners Front | 14:30 | 237 | 40, Line | 
| -- | **TOTAL** | 91h | 1270g | 
