## CAD / Etc Notes for New Clanks

I've grown to really like this design flavour, and I think a stable 'Clank' set of gantries (XY, Little Z, or Cantilevered Z) is a good move, going forwards. These would be configurable in wide ranges of sizes, but serve N17 motors and mostly 20x20, 20x60 size extrusions. I can see adding a 20x40 Y-Gantry modification for a 4x8' ish plotter as well.

The big next step is adding a tool changer / interface, and end effectors at Clank size. This would involve also modifying the XZ-Union to actuate the tool changer, not directly mount into a spindle, same for the X Carriage on the CZ version. 

A larger 4x8' machine might also want more involved support along the length of the Y-Rail, into slotted fasteners on the extrusion - hence the 20x40 Y Rails.

Finally, long runs really need some flanged idlers, here's a video:

![long-run](video/2020-09-09_clank-long-run-belt.mp4)

This is a 'fine' amount of friction on kevlar core belts, but not cool with steel core belts, which I would like to use for longer runs / better machines anyways. I think the whole clank set should get idlers in that case, but that update will come after HTMAA 2020. 

Final note: I want to have better tensioning mechanisms... that would mean dropping a screw-to-tension machine screw in someplace, or maybe the cooler take would be to leave the design as is more or less, and build a little tensioning wrench that wraps on a motor, inserts an M4 SHCS with a nub on the end, that pushes on the top plate. Install that, hit el tensione, and apply mucho torque on the motor mount SHCS (washers!) to fix in place. 

Smaller note: consider a plate orientation swap on the YL and YR carriages: all three extrusion thru-holes on the same plate, the motor element tacks on the side. 

## Small Clank (not clank)

For HTMAA 2021, it would be cool to do a little micro circuit mill - task specific, very cheap, easily reproduced: then write a little artefact paper about it. 

- probably corexy w/ smaller leadscrew z 
- try NEMA8 spindle motor, maybe possible w/ new closed loop stepper code? 
- maybe NEMA14 XY motors 
- try to use plastic thread-forming screws, no insert nuts or heat-set 
- spindle: also try spindle-bit-itself on rollers, with motor & i.e. o-ring friction drive... a kind of bit-direct kinematic mount

## Small Spindle

a-la:

![rollers](images/2020-12-17_roller-spindle.jpg)

- no collets / etc 
- 625 or similar bearings locate the endmill in a kind of 'rolling vee' - two near tip, two up top, for 4 points of contact 
- NEMA8 or similar tiny motor has o-ring roller drive, this is 5th point of contact, preloaded into the endmill. 5 pts define all but one (rotation) 
- NEMA8 swings away from the assembly, for quick tool swap. 
- drive roller is angled *slightly* upwards, on spinning angle, so it drives the endmill into a flat (metal / high hardness) stop to prevent Z motion 
- cutout tools maybe exclusively down-cut to prevent tool pull-out during (relatively) heavy machining 

## Micro TC

I think actually the 'pinching' tool changer can be made pretty small. One way to think of that shrink is just going from M4-hardware-scale to M3. For light duty loads, a latching (non-actuated) tool-loading mechanism is probably possible, a clip / preload similar to what is sketched in the roller spindle above. 