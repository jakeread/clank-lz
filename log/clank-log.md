## Clank Log: Jake's Rough Notes

### Current Status

clank-lz kits in the wild 

### Software Bugs

Since software for this 'roll' is all over the place... known issues list here:

- notes in 'virtualMachine.js' on promise timeouts 
- try i.e. sending a 'setcurrent' command while motion is ongoing... seems like some hangup is going on, but should be able to parallelize those 
- the PWM/5V/GND label on the esc board is backwards 
- setcurrent & other (?) ucbus chb transmissions appear to drop the last byte, or something like this - so i.e. remote z current resetting doesn't work 

### Hardware Bugs

- next time, do phenolic bed for tape stiction, or get a vacuum chuck eh

## Hardware CZ Deltas

- idlers are 13.5mm tall
- some BHCS M4x25 are now x30
- motor plates 5mm -> 6mm,

**Y Belt Position**
- Y belt pitch line no longer 0.5mm inside of Y rails, now 2mm outside of exterior line of rail 
- center of belt is 17.9mm from top edge of rail 

## 2021 01 04

It's you! From 2021-01-04: rm'd all git history locally, to free space. TODO for you is scraping issues from gitlab, making a notes-of-how-it-went report for the future / next time you do this, and pushing this clean repo up, w/ report. 

## 2020 11 12 

Toolchanger is up now, bueno.

![machine](images/2020-11-12_clank-cz.jpg)
![change](videos/2020-11-12_clank-tool-pickup.mp4)

## 2020 11 11 

Prints are set to actually finish a tool changing experiment, but I need to roll a controller. I'm wired up in an ad-hoc way, so can do firmware etc. 

I think motion will be the same as it has been with clank... some Z mm / step calculation should happen, and the big delta will be adding a 'tool swap torque effort' code to that motor. Here's a case where some more robust bus virtual routing would be great: the b channel is actually feeling a bit broken at the moment. 

So I guess I break that out & prototype it on its own. 

## 2020 11 10 

Finally getting the CZ ready to move, have noticed that the Y belt clamps are *not* aligned with the YL / YR belt center line, so I need to correct that tonight. 

Those are printing. To route cables, I probably want a few more little bits. 

- plant bus-droppers on corner posts
- get PTFE tube take-off perp or parallel to extrusion 
- example seat / pickup: maybe spindle ? hotend ? 

Samuel's extruder is pretty good, though I will want a hotend circuit (thermistor, heater, probe), and the mount to the toolchanger: maybe more integral than is possible to just tack on. 

## 2020 10 30 

Through the list of 20 or so small updates I noted in the build yesterday. 

## 2020 10 29

**Clank CZ Build**

Ok, I'm into the foreign territory on this build. 

I suspect I will want stronger 'stops' for the open / close on the tool changer... getting that cable to work well is really tough. I think I do cable routing / setup near the end of assembly. 

Did the X and Y, guess I'll struggle through the Z now. 

Z a bit janky going together, but feels tough once its all cinched. Without the motor, belt drives etc it's 1361g. All together, it's 2350g. It does not hold static when the motor is doing no work. Given the complexity, it might actually make sense to do the floating XY stage type machine... but at a later date. 

Sort of bungled the pull-pull cable install, but it should be good enough to work. The beta on doing that is to actually be really careful with the last length set... I measured and then it slipped some as I was crimping. Also the two lengths are not set to be very similar. Luckily this isn't a precision thing, so it's fine. 

![assy](images/2020-10-29_clank-cz-assy.jpg)

## 2020 10 22

OK, have front brace now: easy little tack on. I think I just have to setup to print, then I'd be ready to build: although there's still all of the CAD to make an actual swapping end effector. Am I waiting to do that? 

## 2020 10 21 

Aiming at the deep integration on these today. I think it can feel good... have a lot of tricky little fixtures though, might have to get involved w/ M3 hardware... hopefully not M2.5. 

![b3](images/2020-10-21_beyonet-integrate-03.png)

This is looking a lot better. I still have to build out the the rotary element, coarse alignment taper, some way to secure pins, and the cable drive. 

| HW | PN | Where |
| --- | --- | --- |
| Dowel 6x12mm | 91595A440 | Pins | 
| FHCS M3x20 | | Beyonet Rear Cap | 
| 0.047" Wire Rope | 3461T632 |
| 1/16" Wire Rope | 3461T633 |
| 0.047" Rope Stops | 3926T43 |
| 1/16" Rope Stops | 3926T44 |
| 1/16" ID 1/8" OD PTFE | 5239K24 |
| 3/32" ID 5/32" OD PTFE | 5239K25 |


I am going to try to insert the pins thru the rear:

![b3](images/2020-10-21_beyonet-integrate-05.png)

I also notice on my beta changer that the ramps don't do a great job of dealing with varying hold-down lengths... i.e. actually the ramps are over-constrained, so I need some flex in here. 

![flex](images/2020-10-21_beyonet-integrate-04.png)

Put some little seat flexures in here, now I need to do the cable situation, then I'm done w/ this and on to the cable driver. 

OK, current / last (?) issue with the head is that I've just run cables through the top pins, whoops. Solved that.

![flex](images/2020-10-21_beyonet-integrate-06.png)

Highlighted the cable run there... next I'm into the drive element. 

OK, done w/ the reduction.

![flex](images/2020-10-21_beyonet-reduce.png)

Tomorrow will fix the fusion model, add the braces, setup the next round of prints. 

## 2020 10 20 

Would be aiming at getting through this this week. Designing the Y rail ends / guides currently. 

- make a clamp-block: 2 bolts to pinch, set width / etc 
- might be better to sit the flexural side interior: the exterior wants the stiffness for belt tension. 

Doing the XY lift causes some growth in the XY footprint: have to attach guides to the ends of the y, increasing length there. Lifing the bed, though, doesn't do this. With the bed, I should only need 3 motors to lift as well. And this property of the thing dropping *away* rather than *into* the work is kind of nice. Toolchanging faster / potentially less awkward. XY Belts maybe stiffer. Honestly, goddang, probably the heckin move. 

I could honestly also give up on this excursion: it might really be that the cantilevered bed is the stiffest way to do this. Bearings into the end-posts of the frame. It's also, like, done. Heck. 

But also: https://www.youtube.com/watch?v=PYYgrNodnE0 

! Arbitrary decisions are tough. 

Yeah, really, I (again) need to go forwards on this, not backwards. I'll make this cantilevered machine and get into milling stuff there: there's so much to do before I can try HSM paths and see if I'm even in a ballpark of being able to do it. The really impressive thing to do there would be to use the newly sensitive & hp motor drivers to optimize the feedrates & accel when running HSM. 

So, for now I'm going to pin my toolchanger onto the X Carriage. 

Working through the beyonet, I realize I can take about ~ 8mm of thickness off of this thing, though it might need a medium size CAD re-work - oooor no I can hack it. Not bad, and actually that was just ~ 5mm of 30, so 16%, will take. 

![beyonce](images/2020-10-20_beyonet-integrate-01.png)

Getting this in here is a bit of a trick.

![beyonce](images/2020-10-20_beyonet-integrate-02.png)

I can certainly decrease the pitch diameter on the drive pulley, then insert the dp into the x carriage's front face. I guess that's the best I can do, though I certainly want it to be tighter, there's just a big ol' stack of stuff in there. At the moment there's 30mm between the back of the tool plate, I can cut ~ 10mm of that out. 

I could probably get this thickness way down if I could maneouver the pin-holding body right into the x carriage, reduce to one rotary bearing, and then drive it via a cable that sneaks out behind the x carriage front plate. Might need to get a bit wider with the x carriage, and sneaking the pins in seems like a bit of a trick. 

It's a big challenge, to get rid of this last 15mm between the x carriage face plate and the tool's rear plate, and for now it's probably not worthwhile. If this all works and controllers become slick, I can double back and do some fancier design... luckily, it is unlikely to require changing anything todo with the tool plate. So I should carry on with a shotgun marriage here and spec a pulley length, motor location, etc. 

I do wonder if a cable drive makes sense anyways though: I don't really want to haul a motor around, getting a whole belt drive in here is a PITA, and I can pull more torque remotely. It means a bit more work - there's still this assembly to wrangle, there's cables *and* belts to tension, and there's the cable drive mechanism. However, a cable drive mechanism is also kind of a sweet little assembly to have, and I could flex bike parts into the system, etc. Will be back at terminating cables properly etc, yikes. 

I guess that's the choice now: do I send it and really integrate this thing, get a cable drive into the assembly, float the x carriage plate a step out etc, put pins in the face, single bearing... or do I just tack a kilo of weight up here and call it? 

Lots of notes and not a lot of progress. I think my patience is just low after today, it's really asking for a bigger integration: I should do it - it'll perform better (stiffer, way lighter) reduce footprint, etc. 

I suspect the way to break it out is to start a new part that's the X Carriage face plate alone, and integrate there. That's going to be some triiicky CAD, but something I need to get through and probably fairly rewarding as well. 

## 2020 10 19 

Today's a reading day but ofc I have some thoughts about this from last night / this morning to get down. 

Mostly, they are that: yes: I like the idea of a floating XY frame a lot: here's a tiny sketch:

![float](images/2020-10-19_clank-float-xy.jpg)

I'm terribly attracted to this idea, sort of unfortunately. Couple of notes:

- makes loosing power a bit more painful, as the tool comes down into the workpiece rather than the workpiece dropping away from the tool 
- makes toolchanging a bit slower, i.e. if the tool lives 'up top' we have to move all the way up to grab it, might be fine 
- on the other side of the above point, means we can potentially fill a larger volume of tools on the face of the machine 
- the bed can be rock solid, which I think is rad, esp considering likely futures w/ experiments / adhoc equipment / rotary tables / vacuum chucks on the bed. 

So the little bits that pin the Y rails to the rear & to the front are straightforward, I need to pick some frame alignments & do the z-reduction calculations, and the largest unknown design element is the z reduction, and tension. Ideally the z-reduction is relatively small (less printing) and simple, etc. 

Awefully tempting to somehow figure out how to get the tensioning into the lower section of the pulley, as the ++ stiffness up top will be preferrable. I could also put motors up top (maybe even better for controller opaque-ness) and tuck a little idler-tensioner in the bottom corners: that's probably the move. 

![float-top](images/2020-10-19_clank-float-xy-top.jpg)

OK I think it's kind of insane but let's see what happens. It's kinematic AF, so it should at least do pretty well for repeatability etc, and the belt limit is sort of interesting. 

The spreadsheet, to calculate my reduction requirement. I am going to do a really rough estimation of weight by weighing the top half of clank-lz, including the spindle. 

My very informed estimate is that each z-corner will experience near to 3kg when the tool is near it's corner. Since I might eventually want to haul a big plunger full of clay around in something like this, or addnl AB axis, I should spec it up to 6kg or so - which is, in all honesty, a pretty good amount. 

| Motor T | Reduction | Drive Pitch Rad | Linear Force | Corner Weight | Weight Force | 
| --- | --- | --- | --- | --- | --- |
| 0.3 | 3 | 6.366 | 141.4 | 6000 | 58.8 |

So it looks like just a ~ 2:1 will do this, and a 1:1 will barely do it, with no headroom for accel. If I just pick a torquey stepper and promise to do really good control with it I can probably just use lots of motor current instead of the reduction, but, yeah, want to be moving this thing around with oomf: should aim near a 5:1 reduction. With that reduction, and a 16t pulley, I can do a small-ish 50mm pitch diameter output pulley, and if I elect to use the same 20t pulley as in the linear drives I can do 63mm. However: 6mm wide 16t pinions are extremely widely available, so I should pick there. 

Checking against the GT2 max torques, I need a 28T output (not 20T as above) to handle this output torque: 20T is rated to just 1.5Nm, 28T for 2Nm. That makes me something nervous though: of course I'd like to apply ample force to accelerate the Z around, not just enough to avoid motor stall when it's hanging around. 

I wonder if I'm signing up for something attrocious here: tuning z motor PIDs might be a PITA because required holding forces will sometimes be large and sometimes very small (depending on variation in the position of the head). I'm inviting in four pulley runs where there was one previously, but am reducing frame complexity, and am also introducing some baseline-stiffness-level tramming where previously I would have been tramming the bed on some kinematic coupling between the z cantilever and whatever bed framed it out. 

I think this remains appealing because I want to do a kind of datron milling on this platform, and I think ultimately it's a bit better for that application. 

Before I can really proceed designing the Y rail end-caps w/ rollers, I need to know where the belt is going to live, and make a framing plan. Longest lead is ordering extrusions, so I should try to get to that before I put prints down. 

The flying XY is also going to be tricky where dropping-during-power-off is concerned as, again depending on where the tool is, some sides are likely to drop ahead of / faster than others, causing some ugly lock ups. 

Have reconfigured this again to make a larger output diameter: 40T instead of 20T, to avoid the near-limit at 20T where GT2 pulleys are only rated to ~ 1.5Nm, which was the output torque there. A 40T pulley is rated to about 3Nm, where a 6:1 reduction means I'll max out around 1.8Nm, so I should be save. 

I wonder if I just go in on this thing tonight... the corner posts are actually a bit of a trick once I have to get belts etc to fly past either end. 

Need to know where the belt is going to live, can probably just set it now: will say pitch line is 5mm from the interior extrusion edge. 

I think I have a plan for the belt gripping... Altho maybe this is not the move. Tomorrow I'll get back to it. I think it's not this rotate-around thing, it's just two separate clamps on a longer belt run. 

## 2020 10 17

Tonight... getting CAD prepped for this. Big deltas are:

- flanged idler update for XY stages 
- toolchanger on the X Carriage 
- spare tool 'braces' in front

I'm tempted to revisit the floating bed notion, this cantilevered thing seems like a lot. I think it's only reasonable with lead screws, and yeah, nevermind... the Voron &ect move the XY platform... that's appealing if I wanted to ever get a vaccuum chuck on the bigger platform and actually have a tiny milling machine, though it's a total kinematic nightmare. I think this is the same trap of wanting to have one machine that does it all. 

Maybe the eventual & responsible move is to put a small ballscrew in the bed. I should build one to see how it goes, as is - I expect some bed slowness, but better to solve problems when they appear. The drive here is to get to toolchanging and do multiprocess stuff: this is mostly a printer, the bed should be fine, lots of other work to do. 

- YL, YR carriages get flanged idlers 
- X Carriage gets big idlers and beyonet integration 
    - this might mean pushing Y belt stacks out -> 
- z stepper ++ reduction belt slack 
- extrusion - to - print clearance is tight in most places,
    - have updated most of this in YL / YR already: do same for corners, for z stage 
- z beltbox is tough to assemble says samuel... check check, plan 

I'm going to fork -cz from -lz in fusion, because this isn't worth it. 

OK, am getting through the YL and YR, which will be trickier because addition to the idler stack pushes the motor in towards the X Carriage, where they are otherwise tucked... 

This is coming along OK, am onto the YR... done with the XY Carriage now, save for X + Toolchanger additions. Fusion really letting me down with large assemblies here. 

If I can get the z-revisions through (there aren't that many) I can get print plates up and send them at the lab, then move on for the day. 

**ready to print**
- yl, yr 
- z stage 

**needs work**
- xcarr ++ beyonet 
- rear corners: watch yl rear stop 
- rear & front corners: belt center line position 
- front brace extrusion for toolchanger 
- bottom corners should get hole for the foot part 

**consider this**

had been really not-liking the idea of lifting the whole XY stage, but I was thinking about trying to kinematically mount some big plate. It's not actually this: I can do YL and YR rails on their own situations: just as the X Rail 'floats' on the YR and is 'fixed' on the YL, I can fix the tip of each Y rail on the front of the frame, and float the backs save for rotation. Then each corner post gets its own N17 & GT2 train. It's kind of madness: I'll have 6 motors just for the XYZ system, one more for the toolchanger, etc. If there's $20 per motor driver, say $30 once you pay for the motor as well, this is $200 just in motors. Each one assumes some hardware, pulleys, etc: the z-drives are, lets be honest, probably reduced belt drives. Could do the spreadsheet on that one at least, though... maybe they could go direct. 

Also, it's so cool haha - I want it because of this. Is that a sin? 

## 2020 10 09 

A couple of sketches of the tool-changing version of this machine:

![tc1](images/2020-10-09_clank-cz-tc-01.png)
![tc2](images/2020-10-09_clank-cz-tc-02.png)
![tc3](images/2020-10-09_clank-cz-tc-03.png)

## 2020 10 05 

Class Kits are 'done' and released, and my next move is to get a Clank-CZ together, with a tool changer. I'm on with Samuel today who's built one, and might have some CAD deltas. Then I can try to:

- update Clank-CZ assembly to have flanged idlers on Y and X carriages 
- integrate tool changer (automated && hand-op) into X Carriage 
- some trouble with the z-belt... check clamp spaces 
- z-tension-pivot kind of a PITA 
- z-belt: should feed through easier, wants to come out on the wrong side of the lower roller 
- z-frame / belt clamp might interfere 
- z 'bottom y constraint' a bit tight to assemble as well 
- put some feet on that thang 
- z belt motor -> reduction should get some more free space 

## 2020 09 29 

Finished printing tonight. Here's the log, in case I ever want to reference how it worked out:

### Printing Tables

| Plate | Contents | Time | Filament (g) | # Done | Infill | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| 01 | Spindle Motor Interface | ~ | ~ | ~ | Revision TBD |
| 02 | Spindle Core | 14:59 | 228 | 2 | 50 Grid |
| 03 | XZU Lower Plate | 8:40 | 93 | 3 | 50 Grid |
| 04 | XZU Upper Plate | 9:26 | 124 | 3 | 50 Grid | Solid Modifier at Motor Mount |
| 05 | YL YR Plates | 15:13 | 179 | 2 | 50 Grid | Solid Modifier at YR Top Bridge |
| 06 | Misc XZU Face, YL Side, Y Belt Ends, LMU Clamp | 11:13 | 122 | 2 | 50 Grid |
| 07 | Corners Rear | 16:57 | 288 | 3 | 40, Line | 
| 08 | Corners Front | 14:30 | 237 | 3 | 40, Line | 
| 09 | PSU-Mounts | 8:45 | 145 | 2 | 40 Grid |s 
| -- | **TOTAL** | 91 | 1270 | 

| Day | Plate | Start Hour | Finish Hour | Fil. Used | # Complete ? | Spool |
| --- | --- | --- | --- | --- | --- | --- |
| 0 | Spindle Core 1 | 1400 | + 0700 | 228 | 9 |
| 1 | Misc 1 | 0800 | 2000 | 350 | 9 |
| 1 | Corners Rear 1 | 2000 | + 1300 | 638 | 10 |
| 2 | Corners Rear 2 | 1300 | + 1000 | 926 | 10 | - |
| 3 | XZU Lower Plate 1 | 1000 | 1900 | 93 | 10 |
| 3 | Misc 2 | 1930 | + 0600 | 215 | 9 |
| 4 | XZU Upper Plate 1 | 1000 | 1930 | 339 | 10 |
| 4 | Corners Front 1 | 2000 | + 1100 | 576 | 10 |
| 5 | Corners Rear 3 | 1100 | 0440 | 864 | 9 | 
| 6 | XZU Upper Plate 2 | 0930 | 1930 | 988 | 9 | - | 
| 6 | Corners Front 2 | 2015 | + 1045 | 237 | 9 |
| 7 | XZU Lower Plate 2 | 1030 | 1930 | 330 | 9 |
| 8 | XZU Lower Plate 3 | 1030 | 1930 | 423 | 9 |
| 8 | Corners Front 3 | 2000 | + 1030 | 660 | 9 | 
| 9 | XZU Upper Plate 3 | 1030 | 2030 | 784 | 9 |
| 9 | YL YR Plates 1 | 2100 | + 1200 | 963 | 9 | - |
| 10 | YL YR Plates 2 | 1200 | + 0300 | 179 | 9 |
| 11 | PSU Mount 1 | 0800 | 1800 | 324 | 9 |
| 11 | Spindle Core 2 | 1800 | 0930 | 552 | 10 |
| 12 | PSU Mount 2 | 0830 | 1830 | 697 | 8 |
| 12 | Spindle Core 3 | 1930 | 1030 | 925 | 10 |
| 13 | PSU Mount 3 | 1100 | 1930 | 1050 ? | 10 | - 
| 13 | Misc 3 | 2030 | 0730 | 122 | 8 
| 14 | YL YR Plates 3 | 1430 | + 0500 | 301 |

| Day | Plate | Count Started |
| --- | --- | --- |
| - | Spindle Motor Interface | 5x4 | 

## 2020 09 24 

Waiting on the last hardware from McMaster, the spindle hardware from McMaster (which both should show up tomorrow). 

## 2020 09 23

Trying to finally finish this spindle hotfix. Have it together, bearings get a bit crunchy - probably would do better with flanged bearings in that brass bushing, I figure the crunch is bearing misalignment. Whatever, it's quiet enough in this revision. Ideally I would modify the motor interface part some, but it's already printed and I'd rather not bother. Ah - I can shrink the spindle core some, it'll do the same realignment, then those are yet to print. 

So, I think this means I can head into lab once I get this thing assembled on the benchtop here. 

Have flashed bootloaders onto 64 modules, should also do steppers, but will start with SMT assy of the PSU-Breakouts. I think I will omit the filtering there, just put one TVS diode on and fill in two of the LEDs, for a clk and error indicator: these will get the bad modules. Then the ESC breakout I can actually just send with the module. 

## 2020 09 21 

**also** the shaft... bearing interference is big, I probably need to heat these up to get them through, so I should try to assemble the hot plate tonight (if I get through the UI) to heat & press. Given that, I think it means pre-assembling them all... and might mean that I should get that bronze bushing in here.  

Trying to get through the spindle hotfix today, and test the new circuits. The hotplate needs to wait until I have thermal paste for the plate-to-plate interface, otherwise I could be screening boards as well. 

I think the bearings on this new spindle really need to be pressed in, as well. Heating the bearings up helps some, but not enough. So I need pressing tools & stops etc, 

Found a press part, 1ft steel tube 6100K357 with fairly tight tolerance 0.261" ID, another candidate 6100K354 has 0.256" ID, might be too snug up, might be perfect. 

So that's set, and I will want to print a little tool to do the pressing with, to set heights. 

Measurement says the shaft makes it 8mm into the rigid coupling, about on par with SDP-SI's drawing for 0.31" (7.87mm), so I'll spec extension past the bearing head to 7mm, so that it's surely snug up. Tensioning screws are #2-56, that's 5/64" drive size. 

All told, the tool then extends 49mm from the final spindle bearing face. Though this is not good at all for runout, I think there's enough z-travel for it to work. So then I just want to draw the press tools that I'll use, and get those on the printer tonight, then I can get into testing the new circuits. Since it's close, I'll wait until I have the new spindle to really put the whole system through its paces. 

I think that's pretty simple, I press the lower bearing into the spindle housing, then put a tube support underneath, that bottoms out when 7mm of shaft have extended. I then use another tubs up top to press the second bearing down over the shaft and into the top of the spindle housing. 

OK, so that's set up, McMaster is out, and I just have to print the spindle body and press tool to test. 

So I can move to testing the new motor board. 

Have found one error already, in the 2x5 JTAG header - have connected GND and VCC on the header, odd. Concerning! 

So I want to compare the two circuits and note code deltas... then get it on the bus and walk a motor around. I think there are no code changes, just those indicators... ah, no, the new boards have smaller sense resistors. 100mOhm, previosly were 200, so a 2x current scaling. 

OK, works, that's a sigh of relief. 

## 2020 09 18 

OK, I'd say the VM interface is ~ done, so I am into trying to use it, right? I still can't jog with the UI, so I should probably do that... arrow keys? Shift for big increment (10), ctrl for small (0.1), alt for z? 

Ah - I still have to properly do a reset-pos, although I think maybe just querying existing pos / transforming on that is actually the move, no? But the motors... nah, I'll make that work. 

Eh, idk, I think just offsets-from-machine-position (queriable) is the best way to do this. Resetting internal state: odd, especially with this stream ongoing. 

Some last odds then - I think I have the offset correct, but the positions I'm reporting at the motors are reversed. Can flip that... 

Oh lord hang on, wasn't updating the motor firmware. Let me try remote-resets again. OK, those totally almost work, but on the restart there's a knock. It would be awesome were there not a virtual offset, that always introduces pain. 

OK, hell yeah, that works - no offsets baby. Virtual machines == real machines. 

So, jogging. I want a pad... click-in / click-out, pad color is statemachine state. 

OK, I think this is working alright, but I am seeing some slow jogging because of the downstream queue's wait-for-buffer-to-fill operation, so I should add a downstream switch to disable that for jog moves. 

I can do that by setting the `force` option in the conveyor for smoothieroll: Conveyor.cpp line 223. 

Alright! jogging: works. Not a bad little statemachine to handle all of that UI. 

## 2020 09 17 

P. much screwed for the spindle collet holders arriving, so I'm into designing something new. I just need:

| 5mm Motor Shaft | Coupling | Shaft | Bearings | Rigid Coupling | 1/8" Tool |
| --- | --- | --- | --- | --- | --- |
| Motor Shaft | 9845T1 (5mm), 9845T1 (1/4") and 9845T11 (spider) | 1/4 x 3" Rotary Shaft 1257K113 | R188-2Z Bearings 57155K387 | SDP-SI S51FCZ-125250 | 
| - | 49.21 | 4.32 | 14.20 | 26 |

The 1/8" tool to some metric shaft is the trickiest part... ok, can do fairloc 1/8 -> 1/4" part ($26 on its own), then 1/4" shaft, some McMaster bearings, and a $50 spider coupling. All up I'm adding $93 per machine, about 3k total. That's ahn yikes. 

Well, whatever, it's going to happen again. Here's another case where a toolchanger design would be cool, I wouldn't be worrying about this redesign interfering with the z axis, but it doth. I also think the grommet-mount motor is cancelled here... doesn't quite fit now that the thing is so squat: 

![squat](images/2020-09-17_squat-boy.png)

Relative the original:

![not](images/2020-09-17_not-squat-boy.png)

It's cute! That's right! So I basically need to re-think the upper half.

This is done...

![ok](images/2020-09-17_spindle-hotfix.png)

## 2020 09 14

Going to see about speed control on this spindle today... those notes in the osap log. 

The hardware for that is working (timers, interrupts). It being 830, I'll draw a new printed part to get this mounted on the machine tomorrow. Motor base to spindle core top should be 50.5mm. 

- needs M3x5mm SHCS for motor -> PCB mount 
- solders on tall, maybe should do these for students 
- zip ties in the kits 
- bullet connectors? 

## 2020 09 09 

Noticed this on longer runs:

![long-run](video/2020-09-09_clank-long-run-belt.mp4)

I figured as much, and will make a modified X for this experiment (here it's in the stepper dyno), to add idlers on the X Carriage. The rubbing is 'fine' with a Kevlar Core belt, but pretty unacceptable when I install steel core belts. 

## 2020 09 06

Wrote the print schedule today, and started the adventure. 

Protip for print farming: have half of the plates ~ 14hrs, half ~ 8hrs. 8hrs you can cycle morning-start, eve-finish, and then 14hrs go overnight. 

## 2020 08 27

I've labelled things today...

```
7mm tall Microsoft Yi Baiti Bold
    ASSEMBLY BODY-NAME
    5mm from edges, if you can 

5mm tall Geniso
```

Today I want to publish enough documentation here to make remote builds tenable, and set CAD reference for students. 

- clean up the BOM, and link,
    - make esc and psu breakout projects git
    - add these to bom 
- do build.md and link 
- do circuits... somehow, and reference here ?

## 2020 08 19

Spent the last month getting through a totally new roll for motion control, and I'm satisfied with performance, though it came at the cost of modularity. That's OK for now.

![milling](video/2020-08-19_clank-mill-system.mp4)
![sys](video/2020-08-19_clank-mill-beauty.mp4)

![img](images/2020-08-19_clank-mill-02.jpg)
![img](images/2020-08-19_clank-mill-03.jpg)

I missed on the milling height in some places, this was with an unfinished bed and no probing routine. Building that routine, and homing systems, and UI that others can use is the next tick on development cycle. Otherwise, traces look pretty good.

![img](images/2020-08-19_clank-mill-micro-01.jpg)
![img](images/2020-08-19_clank-mill-micro-02.jpg)
![img](images/2020-08-19_clank-mill-micro-03.jpg)
![img](images/2020-08-19_clank-mill-micro-04.jpg)

In open loop, the spindle goes at around 22k RPM, but a motor update will change this...

![rpm](images/2020-08-18_clank-spindle-rpm.jpg)

## 2020 07 30 

Assembling the 'final' rev of the spindle and machine today.

There's some small variance in the collet holder ODs, meaning that their marriage is sometimes easy and sometimes seems like it needs a reall press / tool. Luckily this is possible with just a bar clamp... I have some photos of this. 

![spindle](images/2020-07-30_spindle-press-assy-01.jpg)
![spindle](images/2020-07-30_spindle-press-assy-02.jpg)

Trick with Clank is that the bed clearance is not actually 60mm right now... want some smart answer to that challenge, otherwise it feels good - should see about testing the spindle under the 24V PSU today as well. I guess / feel that the obvious move for this is to machine one HDPE plate-bed for each machine, and push that down to the 60mm. Will play around with it. Could make it all nice and flush against the mounts, then, and use some SHCS rubber caps as feet. 

OK, assembled. Feeling slow. It's because I'm not actually here to finish this yet - my next working steps are about getting some accel control in the motor. So, I'll take a picture of this, think about going 'CAD/BOM complete' for the beta builds, and then test out the spindle to see what's up / likely outcome of that. 

![clank](images/2020-07-30_clank-lz-fab-01.jpg)
![clank](images/2020-07-30_clank-lz-fab-02.jpg)

So... I'll put screws in the CAD, but I also should think about this bed. Screws / hardware are in and I'm pretty sure that's all BOM-aligned. So! The bed. 

I think I'm just going to elevate the corner posts, no big change to the layout. This will satisfy mould making, and will make height-from-bed easily configurable, and keep the cost down - not adding any more extrusion / bracket. We'll have to spec a big thick block to put down for circuit milling, to lift the bed back up. 

OK, that's all done. Here's the beta model, containing all components, etc.

![betabuild](images/2020-07-30_clank-lz-beta-build.png)

## 2020 07 28

Revisioned the Z-Drive on this thing, and have things printed now to go again. Here's a quick picture of the current state, about to update:

![img](images/2020-07-28_clank-lz-close.jpg)

- bed -> tool base height is currently only 26mm. wants 60mm for wax milling.
- do by: 1" HDPE gets face in front for PCB milling, flip base to find pocket in back for wax / part milling ? 

## 2020 07 20 

Today I want to get through the Z, setup prints and orders, doc (incl walk photos to clank-log), and then cleanup notes below, and return to programming. Perhaps I should just try shipping step codes to ahn motor, in x, see what happens / how saturn behaves, etc. 

Alright, thru with the MD. Now just want notes, orders, doc, BOM... be prepped for Wednesday meeting. Thing is going to wrip. Might consider deleting the frame's posts... I guess it's certainly worth a shot, probably saves some cashe and makes things hella simple. Definitely, ideally, there would be a slick way to adjust the relative bed height... This saves $27 in the frame assembly. I think it's probably the move. 

I could even delete the lower frame entirely, mount prints to the bed surface. Do bed surface plate in ~ phenolic? 12x12 sheet is ample space, is $38 on McMaster. That deletes the frame and the HDPE, to save... $36 total, and some assembly time. Adds probably oodles of printing, and might get wobbly. I'll have parts to try both, though. The same size HDPE sheet is only $14, to save another $22. Or consider this: 1" thickboi(tm) HDPE base. 

The front can pin through a tapped hole in extrusion, the rear brace should just mount up to the side of extrusions, reducing size and allowing the frame to be 'slid' out from the bed - or the bed to be slid out: if this is sacraficial, order +1 new, slide new bed in, ur done. 

OK then mostly I need to figure how to attach the bed to these braces. I guess we'll end up chopping big HDPE sheets up on the zund, but it would be cooler if this were easy with just ordered stock, so I'll assume that. I could use self-drilling screws, or thru-mount some M4s as is otherwise machine tradition. Width tolerance on the McMaster sheets is just 1/16" - pretty loose. Thickness is +/- 0.05" - a whole 2mm spread. So the bottom plates should be separate as well. Eh, scratch that - the belt clamp is too integral here. 

So! In the end this is 8mm linear shaft, 2x LM8UUs to guide. It's integrated with the X Carriage, and so should be lighter, a bit stiffer, and besides - much eaiser to assemble. 

![lz-render](images/2020-07-20_clank-lz-render.png)

The last note - I swapped the bed / frame clunk for four absolute *chonkers* of 3D Prints that bolt direct to an HDPE sheet / bed at 12x12x1" size. This also reduces overall complexity, BOM size, BOM cost, etc.

## 2020 07 19

I've assembled the Z-Drive today and there's enough play in the bearings (not preloaded enough, low confidence that I can apply enough to avoid bearing walk without causing trouble).

![zd](images/2020-07-19_clank-lzd-01.jpg)
![zd](images/2020-07-19_clank-lzd-02.jpg)
![zd](images/2020-07-19_clank-lzd-03.jpg)

Here's a clip of the wobble:

![wobble](videos/2020-07-19_clank-lzd-squish.mp4)

Assembly for Spindle / Z Drive
- also open up the M4 hex pockets in the motor interface 
- maybe some other way to to motor mounting, overall this is awk
- if you ever use this little-z iteration, needs intentional breathing space for the lower flexures on the x carriage 
- getting the z-nut onto the spindle: tough. 

Notes for Spindle / Z Drive
- the simple preload-into-90deg-bearings isn't *quite* enough constraint: the spindle easily rotates out on one bearing face... i.e. preload as it stands isn't enough. 
- it is *cute AF* though and I'm pleased with that. I think on an integrated xcarr setup, this could really rip. 
- so I think the move is to setup for the integrated system, 
  - 20x80 main beam 
  - zd plates ++ xcarr plates are *o n e*
  - the 12x150mm rails to go std issue 12 / 8mm rails w/ linear bearings
  - still a small preload leadscrew (which now only handles rotation) against the rail, to ensure linear bearing slop eliminated 
  - maybe order stock 20x80 extrusion, chop saw 
  - if I do this work now, I can be more or less hardware ready for aug. 1 for Rob & Squad to start demo-ing a machine, w/ whatever controllers 
  - or hot damn, maybe I get a proper linear 'rail' hiwin thing 

12mm linear shafts are less commonly available in short throws, i.e. the 150/200mm I want for this. 12mm *rails* seem available around this range. the 'rails' tend to be much wider spread in terms of quality: many amazon orders will be trash components, I think the shafts / roller bearings are more consistent. On McMaster, linear shaft 12x200mm is $8 and bearings are $25 each, the 12mm rail with two carriages is a full $250, about $100 of which is the rail. I think that's the move for this project, it's not *wicked* cheap, but it's close, and cheaper alternatives are mostly available, i.e. swapping in amazon-purchased LM12UU parts ($10 for 4) with the McMaster rail ($8 for 200mm) seems sound. 

So this is the move... 12mm `LM12UU` bearings on a McMaster shaft. I'm going to get a chop saw to boot, so that I can cut Misumi extrusions on my own here, there's just too much lead time otherwise. 

I'm going to do this integrated XCarr move now as well. I wonder if I should go to 20x80 beam, or see about just having a *very* tall xcarr top plate. 

- integr8 integr8 
- 20x80, or 20x60 ? one is less work less printing less parts swapping 
- LMUs on the spindle / z, do you have to split LMUs around support? maybe. extend spindle can? hmm 
- the 12mm rail should be seated in vees, w/ M4 nylock to screw-down-into: reach nylock in when no rail, slide rail thru, tighten nylock 
- might be 10mm rail is the move, to squeeze in here. an in all reality, 8mm is probably enough? 

I think retaining the 20x60 makes sense: I can get fancy in the top carriage to keep the linear rollers spaced 'around' the top plate... IDK my linguistic-center-for-describing-mechanical-design is pooched today pls, I can't do any better than that. 

This is a bit of a trick. It occurs to me also that since no load is coming through the face plate, these little ears I've on the face plate and that whole through-bolting to those issue isn't really applicable. The face plate is still mounting place for the XZ constraing bearings, so I can't delete it entirely. Though the ears did give some extra stability overall. 

I think I've more or less modified the x carriage for this. Lining the spindle up to suit doesn't seem like it'll be terribly difficult, I can pull a big flag out on the right and mount LMUs to that, then just adjust the lead nut to float a little for alignment. I also still need to sort how I'm going to secure the rail in place. 

## 2020 07 16

I'm also thinking it might be 'the move' to spread the XZ-Plane constraining rollers on the x carriage (i.e. the four that connect to the front plate of the x carraige) out *around* the front XY constraining bearings (i.e. the ones that connect to the top and bottom plates), so that the top plate can be more contiguous leading out to whatever z-drive mechanisms are in front of the carriage. This is also more appropriate for racking prevention, especially if the whole thing gets taller (to 20x80mm beam, not 20x60). There might be some awkward maneouvering to do on the bottom here though: those flexural beams more or less need the space they have now. In any case, if you do do this integrated design, take a minute to consider that. 

## 2020 07 13 

The clank-lz / spindle design has been done for a few days, and it's all more or less printed now. Parts are on the way. However, I was still thinking about this in the morning... The load path from the tool to the x rail feels a bit long and wobbly. Now, it's actually fairly obvious where the better design exists: the x carriage's top plate and the z drive's top plate can come together in one part. Their 'print surfaces' are already aligned, so it's actually not much more than a merge and adjust. I think by doing this I could also tuck the z drive in a bit closer to the rail. Now, given the height of the z components, it would be even better were the x rail 20x80mm, rather than 20x60. I might even want to go right to a 40x80mm rail here: but, not for this little thing, really. Modifying these designs to do 40x80 for future large-scale sheet machines: easy. 

Whatever. This current design might actually work OK, I just have some doubts that it is *the right move* in the long run. However, since I think it will be serviceable, I should wait until I have it together to really make an evaluation / gauge how much time I think I should give in to developing a new X Carriage / Z drive. Seems likely that the version of this machine we 'ship' to students will have a more integrated x carriage / z drive situation, and a 20x80mm rail. 

Final note: current z drive is spindle only. With these improvements, maybe possible to make the tool-plate / swap version. However, I think that that is *always* going to add weight & decrease stiffness, there's just not a silver bullet for swapping.

## 2020 07 09 

I'm making some progress through the small Z stage. There's some awkwardness in the top right, where the floating rail meets the X Carriage, but does so pretty far north.

![lzd-01](images/2020-07-09_lzd-01.png)

This is unfortunately a big old plastic beam. I could alternately just grip it 'around the side' as it's loaded in the Y+ direction, and leave the steel rod to do that cantilevering down to the plastic grip-around. Something more like that is probably the right move. I could link that component up with the motor mount, to keep the 'cross ways' tension together. Looks like it'll clear over the right side. 

To 'swap' ends, effectors will get the right-side bearings mounted static, and a 'rotate in' mount to the lead-nut. This won't be all slick hot-swap yet, but that can come later... 

Unresolved is that the motor mount currently overlaps with the upper left thru-bearing screw mount. I can shift the whole thing forwards, then I get an opportunity to clamshell these two pieces together a-la the rear of the XCARR. There's also a chance I'm near the width where I could squeeze the spindle right in here, between the two sides of the rail. All integrated-like. I'm just in at zero clearance, just need to add a few ticks. 

So besides screw selection, this is mostly done, just need the spindle / carriage. It's got a clunky feel, but whatever. Can confirm again that this is best done with the Z and X fully integrated, and perhaps eventually I do that design work, but it'll have to be later on. Also, hopefully, by then I am milling billet phenolic parts or something of that nature - to get closer to real zund-type performances. BLDCs, good stepper control, the protocol, all have to come first. Hardware is the hobby. 

So, moving on to the spindle, which will be a specialty roll / spindle-just-for-clank version. 

Yeah, this works out fairly well. It's definitely not going to be super rigid, but it will be about as small as possible. 

I'll get through this tonight, the *final* hanging item will be about mounting electronics nearby... Then hopefully I can tally up a BOM and rinse myself of this unexpected CAD stint. 

OK, this is done, save for circuit mounting allowance.

![lzd-02](images/2020-07-09_lzd-02.png)

I think this is not bad. When it's all the way up, a long-ish 1/8" collet bit clears the bottom of the z-drive, and when it's all the way down, the same bit makes it to the bottom of the frame. 

![up](images/2020-07-09_lzd-up.png)
![down](images/2020-07-09_lzd-down.png)

I don't think it's as stiff as it might be, but at this point I think I'm really maxing out the performance of the material: the real move, to make really big sheet rippers, would be to start in on 'billet phenolic' parts, maybe machined on this thing. Ha. Also to do for those tools: BLDC drives. All of that is more exciting than whatever incremental increase in stiffness I could get from more printed components. 

So... to really close all of this out, I need to assemble a BOM, with cost. 

- electronics mount on LZD ? don't know what it is yet. put flat face on the spindle itself? easy M3 'threaded' pla, no insert no nut 
- BOM for LZD/Spindle System 
- place components in CAD?
- github.com/azonenberg/pcb-checklist 

## 2020 07 08 

OK, just paring out Clank-LZ from Clank-CZ today. Down in this size, the frame is a bit awkward: z-clearance is small but not zero, working with posts like this seems to increase the overall clunkiness to a kind of undue amount, I think some other layout could do better:

![clunk](images/2020-07-08_lz-frame-awk.png)

But you know what, given the way the YL bearings interact with that beam end, this is just fine. A little clunk doesn't hurt the Clank. OK, fine around 321x361mm machine dimensions. I can trim 31mm of bracket from the front. Bit better...

![lz](images/2020-07-08_lz-frame-ok.png)

So I just need a front and back post, and front and back feet. 

![corners](images/2020-07-08_lz-corners.png)

So, 450pm now. Next tasks would be to design the z-drive, and the spindle. Start at the spindle, to size the ZD? 

Spindle... really just want to model the collet holder, then bring all of the PN's in to the assembly from McMaster / etc. 

Alright, I've that all sorted out...

![spindle](images/2020-07-08_spindle.png)

But I've left the 'mounting to other stuff' part unresolved, that'll integrate with however the Z drive turns out. 

The small Z drive is a trick, definitely. It probably will work much better if I make some amendments to the X Carriage, to help with mounting. Have the things all in-place, will sleep on this and hopefully get through a design tomorrow. 

## 2020 07 07

Am printing / have ordered things for what is now Clank-CZ (cantilevered z), but it looks like we're going to try for HTMAA to focus on take-home machines *just* for electronics, so a milling machine will be more appropriately flat, smaller, and having a carry-along small Z-axis. 

### Little Z Drive

Working on a short-throw z-drive for Clank machines that are flat. First order / kick of the can is to figure what an appropriate drive mechanism is. 

### Rack and Pinion

Misumi sells gear racks, not sure how miniature they get. I've found an appropriate set of 100mm length, module 0.5 teeth at 20deg pressure angle, and 15t pinion:

`GEABS0.5-15-8-K-5` $13
`SR0.5-100` $19

So, what does that mean for drive spec? *Gear Module* is a sort of odd unit, it's the Reference Diameter (pitch diameter) divided by the number of teeth. So our module 0.5 gear with 15 teeth has a pitch diameter of 7.5mm, radius 3.75. So if we get ~ 0.3Nm torque from a NEMA17 motor [like this one](https://www.omc-stepperonline.com/nema-17-stepper-motor/5pcs-of-nema-17-bipolar-59ncm-84oz-in-2a-42x48mm-4-wires-w-1m-cable-and-connector.html), we can expect about 80N of force: that's ~ 8kgf or 17lbf, which is enough as I reckon. 

The other q is w/r/t whether/not this would drop the z when it's powered down. 

I do like this option as it can be easily separated from the kinematics of the drive, i.e. we can use the rack preload to add preload into the static side of the drive's rails, and float the opposite side, all easy. 

### Belt and Pinion

Just like the XY drives on Clank, I could just strap a belt-and-pulley style drive here. This would reduce the BOM the most, also decouples from the kinematics well, and is probably overall the simplest. 

However, the smallest pulley I can get a hold of is 16T, for 2mm pitch that's a 10.2mm diameter, 5.1mm radius, for the same motor providing 0.3Nm torque I net 58N, around 6kgf or 12.8lbf. It's not far off. With a 20T pulley as is fitting for steel core belts (preferred for their stiffness) this would do only 47N, around 5kgf or 10lbf. This is surely *enough* but the stiffness is much less than the rack and I do believe this will drop the Z when the power is down, probably breaking bits in the event that the machine crashes. 

### Lead Screws

The obvious answer is the humble leadscrew. Here's an example part:

[4mm Lead NEMA17 $27](https://www.omc-stepperonline.com/linear-motor/nema-17-external-34mm-stack-04a-lead-4mm015748-length-100mm-17ls13-0404e-100h.html)

Though this is 'obvious' it prevents some trouble coupling kinematics-wise: the lead screw/nut additionally serves as an XY constraint. The quick and dirty way around this is to lean in on it (fig/literal) and use that as one side of the rail, adding just one second rail that pre-loads into the fixed lead-screw side. This increases simplicity, but decreases stiffness some (only 8mm diameter lead screw, not a huge amount of beam stiffness), and loads the nut/screw interface in a side-ways manner, making it sure to wear out sooner rather than later. At the same time, that preload also pre-loads the nut, so the whole thing goes low-backlash. 

So this is perhaps the most elegant solution, has the highest resolution in Z, but maybe somewhat floppy. 

I can get fancy with this and order a Misumi lead screw for 12mm diameter, and couple that to a motor with a shaft adaptor. 

`MTSBRW12-150-F10-V6-S10-Q8` $34
`MTSRR12` $28 

So the bougie leadscrew option (I haven't included support bearings, motor, and coupling) is probably the best but also most expensive choice. 

However, if I limit the length of the integrated motor/screw to 100mm, there's not a huge beam to deflect. Besides all of the other plastic printed parts deflecting, this is probably not a particularely weak point in the system. Given it's elegance and simplicity (and thereby likely small-ness) this is probably the move. 

## 2020 07 02 

Just a few things left. Have the new z-drive, that was most of a working day, some last night. Now just the Z Belt Clamps. 

OK, now the complete z path is here.

![cz](images/2020-07-02_complete-z-path.png)

So, last thing I'll do today is sort the BOM and place orders. Whewp. I'll give it a decent once over again as well... 

OK, caught some final errors in the Z, surely there are more, but it's time to build this. I've exported fab-cads, and I'll go through in Rhino to count lengths I need for Misumi. 

Got'em, goodnight hardware. Beauty shot for the dunk:

![render](images/2020-07-02_clank-xyz-render-color.png)

## 2020 07 01 

I've *all* of the brackets (41 total) in the assembly now, and dang wow that's a lot. I want some feet... but damn, McMaster does not have any M5 studded bumpers. So I have some alignment / foot prints, and I'll stick some cork adhesive-backed bumpers on them: 8771K72. 

So I need some element to set the distance from the rear posts to the rear cross tube. I think I can do the whole remainder of the frame length settings with one setup, two tools: one in front and one in back. 

OK, that's great: those will bolt on to the extrusions, then provide faces to clamp things against as the frame is cinched. Nice. Some extra print time to make 'em, but hey. 

Knocking the Z assembly into the frame is a bit tricky as it's currently set up: I want to use the tapped ends of the H-Frame as a mount point, but they're hard to access with the frame around. The answer is actually simple: I'll bang a cap on those, with embedded M4 Hex, and strap that to the rest of the frame. 

Now, in truth, I would probably stiffen things up if I statically mounted the Z in here. Otherwise I am probably looking at increasing the clearance between the XY plane and the bed (to allow screws between) (also for the eventual z-belt mount). So the trade is some stiffness for the ease of assembly / setup that comes with decoupling them. I would feel better about an integrated move if it were easy to slide the Z's h-frame out the rear of the machine. As it stands, I can't do this because the posts are corners - balloon framed, not cake (platform) framed, to borrow some house building nomenclature. I think, because I need the clearance for belt mounting anyways, increasing there another 10mm isn't attrocious. It does mean that I get a ~30mm gap between the structural bed and the bottom of the X carriage. Eliminating this would mean a trickier frame, and some t-nuts for belt pinching on the Z drive. But that's not awful, and the swap to a cake framed machine doesn't introduce huge stiffness losses between the z and xy stages either. Indeed, it's more of a trade: the squishy bit moves from the z-frame junction to the frame-xy junction. I'll be undoing all of the work I've just done on feet / frame tools. The biggest loss is that I'll miss the XY-plane squaring out brackets in the rear. *Another* note is that as it stands, this frame is a solid structural base also for i.e. a PNP machine. 

Well! This is an unexpected pickle. Sort of in the crux of the thing. 

I think I'm going to continue as planned.

Took a break. 

Nevermind! I'm trying to rationalize this away so that I don't lose work. Really, the slides-out-of-the-back z-frame makes oodles more sense. It's stiff. It's sensible. If I ever want to put an i.e. PNP bed in, I'll do it by sliding two cross-bars in along the same rails. 

So, a trip to Rhino to make sense of a new frame, and then back. 

Yeah, this was obvious. Bless the design cycle. All hail the long walk. 

![pf](images/2020-07-02_proper-frame.png)

This is a real race to the last details, feels like I've not a lot of momentum. 

Alright, nearly there now, 'just' have to make the z drive element. That's a whole sitting on its own. Otherwise I'm at the BOM writing stage, some chance I'll skewer it all tonight. 

![close](images/2020-07-01_assy-nearly.png)

OK, started on Z. Going to sort notes also, then sleep on this. Fun little meche problem, the Z. Looks like it can be pretty.

![drive](images/2020-07-01_z-drive-begins.png)

## 2020 06 30 

Well then, I think after a careful look at the calendar this project is going to de-escalate. It's more likely that we'll purchase handfuls of the extremely-cheap machines available on i.e. amazon, and potentially try to throw new controllers on them. This is partially a releif and partially a bummer - this was going to be a fun experiment. So, I think this week I can aim to finish the design, and see what a true BOM cost would be. 

So, to proceed, I'm going to break the bed assembly up, set width, etc, and make a rail assembly. Get those misumi parts. Then the clamshell, or the frame. 

These brackets - while cheap and beefy, interfere with some bearing runs: I'll have to watch that on all axis.

![interf](images/2020-06-30_bracket-z-intereference.png)

For the top of the run, this is just 9mm and I could probably just drop the bearing. On the lower run it's the full 35mm side length of the bracket. To my advantage, the inner races on the z rails here are the rigid members, so these exterior floaters are really mostly just responsible for constraining the rails during assembly. I could move the lower bearing flexure up as well, indeed that makes the most sense: that bearing could probably dissappear entirely to little effect. Here's the flip:

![b4](images/2020-06-30_z-yconst-before.png)
![apres](images/2020-06-30_z-yconst-after.png)

I'm going to sort the throws now: Z is at 128 mm travel, need to adjust XY to match 256 travel... While I'm here, I guess I'm going to give the XY stage a front jaw, and add these brackets. 

Adding that brace lengthens the machine by +20mm for the extrusion, +31mm for the brackets, and +80mm for the (chosen) end effector allowable width in between the carriage and the brace. So, choices: I could bail on that and rely on the frame's lower jaw to do the structural work, and assume that later on I'll swap in some other kind of brace for tool changers. To be honest with myself I'll not likely get to auto tool changing for some time, and I can always hook more extrusions in to an existing assembly. 

So I'm back to the open face, now I should see about making a frame model, then I can be off to designing / starting to think about all of the little frame elements I'll need. Maybe some CAD completion will come sooner than I thought. 

Pulling the brackets onto the XY frame, I'll also need to move some of the side-plate to top-plate joinery. That's OK on the right side, where there are no torsion-constraining rollers, but it's less cool on the left side. Indeed, I might have to skip that bracket, or choose to give up on 30mm in spare length. I'll try it that way this rev, I'm not interested in making the big adjustments to the YL design, it's great otherwise. One bracket on this side will have to be sufficient. OK, massaged that... 

Next rational step is to touch up the corner posts for the XY, but I think it also makes sense to lay up the frame before this happens. 

Well, I think this is an OK stopping point for today. Maybe tomorrow I can get through the rest of the CAD, that's mostly just little frame nuggets and the clamshell z drive thing. 

![xyz](images/2020-06-30_clank-xyz.png)

## 2020 06 29 

So, had thought that I would be about at Beta Machine stage in mid July, and might be on track for that if this next week goes OK. 

I think the z is finally annealing. For parts cost, I should also be considering using joinery plates (phenolic milled) as corner gussets, I think. Post-assembly slot nuts are $44 per 100, pre-assembly nuts are $35 per 100. Still cheaper than lots of little hidden brackets. 

Although, what the heck:

| Part | PN | Cost |
| --- | --- | --- |
| Hidden Bracket, Sheet Steel | HBLPBS5 | 5.75 |
| Hidden Bracket, Cast | HBLBS5 | 7.13 |
| Tabbed Bracket 30x30 | HBLFSN5-SET | 1.74 |
| Protrusion Bracket | HBKTST5-SET | 2.50 |

So, I guess the tabbed brackets are the cheapest *because* they are the best, so they must mass mfg them to heck. These will be the new frame babies. 

| Comp | Count | Cost |
| --- | --- | --- | 
| Frame | 32 | 
| Bed | 16 | 
| XY | 4 |
| Total | 52 | $110 |

That's a lot just in brackets! There aught to be a better way. Anyways, something to pay attention to later. 

### Bracket Notes

| Bracket Type | PN | Cost |
| --- | --- | --- |
| Hidden Bracket, Sheet Steel | HBLPBS5 | 5.75 |
| Hidden Bracket, Cast | HBLBS5 | 7.13 |
| Tabbed Bracket 30x30 | HBLFSN5-SET | 1.74 |
| Tabbed Bracket 30x30, Element Only | HBLFSN5 | 
| Protrusion Bracket | HBKTST5-SET | 2.50 |

**however** brackets can come w/o set, in which case we need also 2x SHCS M5x10 (Misumu PN CBM5-10) and M5 T-Nuts for these extrusions, (Misumi PN HNTT5-5). The t-nut is $35 / package of 100 (enough to comlete this thing) and a pack of M5x10 Zinc-Coated is $7.73 (McMaster). However, the set is actually still cheaper - maybe this is obvious to Misumi, their business is sorting so the marginal increase in sorting needed to bin these into sets is, apparently, zero. Still though, $70 in brackets is a big single BOM item. 

| PN | QTY | Cost |
| --- | --- | --- |
| HBLFSN5 | 41 | $30.75 |
| HNTT5-5 | 100 | $35.42 |
| SHCS M5x10 | 100 | $7.73 |
| total, packed | - | 73.9 |
| HBLFSN5-SET | 41 | **71.34** |

| Subassembly | Bracket Count | Cost |
| --- | --- | --- | 
| Frame | 26 | 
| Bed | 12 | 
| XY | 3 |
| Total | 41 | $71 |

| Print Set | Print Time (h) |
| --- | --- | 
| X Carriage | 13.5 |
| YL/YR Carriages | 17.5 |
| XY Motor Plates | 3 |
| XY Corner Elements | 11 |
| **XY Stage Total** | 45 |

The Z is really awkward, kind of regardless. I think I might have to put bearings in the tapped ends. That might just have to mean some 625zz's. I can't really do this in good faith otherwise. It makes the framing so simple as well. 

This is the move! Three days of heads on walls. oy.

![z-pin](images/2020-06-29_z-post-pinner.png)

So the lower half is pretty similar, a bit different with the offsets... gotem 

![zzz](images/2020-06-29_bed-less-front.png)

Now I just want some component in the front corner, then I should be done with the bed frame, less the motor and pulley situation (oy). 

I'll have to add in the brackets I'm planning on using for these frames as well, they might interfere with some of the running bearings. Probably also want to build that out as an assembly: rail and runner... and I've to set the width on the whole situation. 

OK, I'll call that victory on the z for now.

![zvic](images/2020-06-29_z-victory.png)

I thought a bit about project management here as well... 

Getting this all done in time for September is gargantuan. Today is June 30, so I have just exactly two months / eight weeks to finalize the design, assembly, **instructions**, and controllers. It's potentially enough for a functioning machine of qty one, but for ~ 50 is very difficult, for less is more reasonable, still hard. 

| Week | EOW Target | Work Plan | Notes |
| --- | --- | --- | --- |
| June 28 / July 4 | Nearly 'Complete' XYZ/Frame Design | CAD Party, BOM Organization | 
| July 5 / July 11 | GCode -> Dead Simple Virtual Machine | JS Party | Orders out on Monday for Complete Machine, Complete BOM v1, Printer Begins Work | 
| July 12 / July 18 | Assembled V1 Machine, End Effector & Bed Design | Assembly, Error Correction and Documentation | 
| July 19 / July 25 | Prepare for Beta Hardware Kits, **Circuits Arrive July 20** | Ordering, Technical Reference Docs, Fab Beta Kit Components. Move V1 Machine with new Circuits | Doc for beta testers is CAD-complete assemblies, assembly-order notes. Sort part identifiers, tag on prints ? Also much embedded programming this week: New stepper drivers, debug motion control. |
| July 26 / Aug 1 | Prepare for Beta Control Kits, Order Hardware for Full Kits | Assembly & Testing of Steppers, Controller Software Touches, Software Documentation, | Hardware for Milling EE & Bed: Design and Fab | 
| Aug 2 / Aug 8 | Kit Order for Everything, incl. many modules PCBA, stepper PCBA (?), Tools / Materials (?) ! | Begin 3DP Farming, PNP if necessary, Software Forever | Retrieve notes from Beta kits, final rev on hardware and kit documentation. 
| Aug 9 / Aug 15 | - | Fab / Kit Assembly / Documentation / Software |
| Aug 23 / Aug 29 | - | Fab / Kit Assembly / Documentation / Software |
| Aug 30 -> EOS Sept 1 | Distribute Kits | 

So - is this going to happen? Two months. Of which, one really needs to be about full on printing & kitting. The staging is kind of awkward: had wanted to get a beta kit out, but the ordering cycle means I need to be ordering *everything* for all the kits at the beginning of August. A beta cycle is probably about two weeks, meaning it would need to start two weeks from now - and in that time I can't finish the machine, controller, and instructions. Probably even if I delete controller from that situation, still couldn't do it. So, besides linear timing from tail to head, that was kind of the variable: where does the beta cycle fit in. To pinch it, I think I'm proposing that I push beta kits and instructions to ~ 3 folks (Rob / Victoria / Nathan, Sarah, Zach(?)).

Getting circuits in here is really pretty tough. I'll get my first run of modules back on the 20th of July, likely. I might need to organize two calendars: one for Circuits / Software and the other for Mechanicals. Perhaps it'd be appropriate to do this on a kanban board. 

## 2020 06 28 

ALRIGHT we're going with the bolt-in z-stage. Things are about to get tall. 

The bed frame is really awkward with extrusions. And I'm worried about costs in brackets. Given that, flip flopping back to the three-point-lift bed. Oy! One motor driver costs 6 brackets. 

I suppose I should calculate holding torque against weight, for three motors lifting on simple pulleys w/ no reduction. I think it's barely there, one N17 doing 0.1Nm torque (safe: might go up to 0.2) on a 20T pulley does 0.8kgf of lift, or 1kgf on a 16T pulley. So the drive favours a better solution in Z, using a reduction on an N23 motor, ideally. This brings me back *again* to a cantilevered bed design, as if I'm going to have this amount of complexity in the drive, I just want one. So I've a design problem on my hands, for the cantilevered frame. 

Maybe it's something like this... using brackets just on the rear subframe, pinning to tapped ends at corners, with one printed block up top and one below, on either side. 

![bed](images/2020-06-27_bed-03.png)

OK, this isn't really happening so far today. Taking a break. 

OK! Have now to settle how I'm going to make these corner braces for the frame / rail on the Z drive.

![bb1](images/2020-06-28_bedbrace-01.png)
![bb2](images/2020-06-28_bedbrace-02.png)

I figure the H-frame rail is stable, need to figure if I try to hang some of the bed structure out behind it, potentially bringing the loaded bearing closer to the mount point on the rear's travelling frame, or keep things in front for some (?) only maybe simpler blocks. 

In either case I think I'll put X constraining sets on both sides, so each rail is completely constrained relative the bed, then the H-frame rails will be assembled *after* the bed is mounted on them, to set their distance to one another. 

Damn this one's tough. Damn. Maybe the mech head isn't really booted yet. 

I'd hate to split the BOM, but 5mm ID bearings would allow me to pin the end-tap through a roller. I think the trick is to think about this a bit farther out... i.e. back to the sketchbook my lad.

OK, that was the move. Now I have this thing that makes some sense. It's kind of joined in one of the strut pins:

![fr](images/2020-06-28_frame-roller.png)
![frc](images/2020-06-28_frame-roller-conjoin.png)

I think that's sane. I think I can do a similar thing for the angled strut, not sure. I guess it makes sense to do that in this drawing, to coordinate frame elements, angles etc. And ah - I've misplaced the loaded / unloaded sides as this stands. Should be OK to fix... 

This is OK but as far as the corner serves to make the bed's own frame into a frame, it doesn't do too well. 

I think I'm going to close out for tonight as well. Might have more luck tomorrow, so far I'm stymied on this part. 

## 2020 06 27

Have been drawing circuits (due to halting no-replies-available error in previous circuits), now am finishing that loop and thinking about completing the Z design for Clank. 

![doodle](images/2020-06-26_clank-z-doodle.jpg)

I had thought earlier that I would want a big planar Z stage, lifting on three corners. Now I wonder if I should reduce the motor count from 3 to 1, and do a big cantilevered bed more like what I had going on the claystack, for reference:

![claystack](images/2020-07_claystack-bed.jpg)

Ah, hmm, and if I go with a walking motor (for a *very* heavy bed overall), I can balance it out the rear,

![hangout](images/2020-06-26_clank-z-hang.jpg)

Who knows. Next step here is to look at this design / the original design in Rhino to figure which is wiser. 

OK! First thing: we're going from 300x300x300 -> 256^3 because it's cuter now and the byte alignment is funny. That's for you Neil. Give me one three bit word stream for the motion control, amirite? 

128mm in z is enough... 

The bed frame is of course the most difficult thing here. I also haven't really thought about drive. I can imagine doing this, maybe with some lighter aluminum struts in the brace. Or damn, maybe the whole bed is a tubes-with-printed-joinery situation? 

Hot Extrusion Alert: brackets are $5 each, so counting 32 as it stands for the frame: $180 ! However, tapped ends cost only $3.60 (for both ends in one rod) meaning that if I assembled the same frame with tapped ends and printed corner braces, $43 dollars addn'l cost. most likely to be some mixture of these two. printed braces are not going to be nearly as strong either, I think. s

I'd also much prefer if the bed could slide out from below. Here's the current frame:

![bed](images/2020-06-27_bed-01.png)

I can add some complexity to the frame, to do this and slide out:

![drop](images/2020-06-27_bed-02.png)

But it feels like a lot. Though, it does also help w/ the 'z travel / z height' ratio. 

It looks like some insane people lift the whole XY stage. Which... maybe? But like, nah. The thing I am drawing can likely work well. I think hanging the motor unit out the back makes sense, though that would preclude the 'drop out' aspect. But, maybe, who cares, it adds some frame complexity that I don't like anyways. Also - homing to zero up top is not wise. 

So, I'm kind of sold on three motors then. I'm a little skeptical that they'll really be able to do the heavy lifting though: consider this is only roughly equivalent to having a 3:1 reduction, as weight is not necessarily evenly distributed on the bed. 

Damn. This is a pickle. I think I need to sleep on it, tomorrow's the day to figure this out. When I think carefully about which drive I believe in, I really think this needs a big dirty reduction and that HTD-3M Belt. It's a nice mechanical design lesson anyways, and being able to sub-assembly the bed and slide it in place might even be easier to go about overall than having to fanagle a bed mid-frame, you know? Ah - I could sneak it up the *inside* of a frame. Will still need to lock out the bottom somehow for the belt tension. 

Damn, or maybe the bed/rails are a unit, and that whole situation gets bolted into the frame. Kind of tight. I mostly worry about setting the far-reaching bearings' distance to one another, with that ~ 0.2mm tolerance to get the preload right. I can stick the whole 'x' constraint on one side, but would have to box it out separately. Perhaps I tap the ends of the long members of this frame, and use those as bolt-in positions. I do like this the most... being able to take the whole thing out: friggen rad. Structurally it can be sound, and I can probably kick it towards something fairly simple. The big reduction will be a move, absolutely, but it's a good one to observe. 

## 2020 06 21 

Working through new motion control today,

![video](videos/2020-06-21_mocontrol-03.mp4)

## 2020 06 19

Finished assembly and wiring today. Very smooth when no motor cogging torque in the way:

![glide](videos/2020-06-19_clank-glide-nomotor.mp4)

And a note of the kinematic decouple-ing-ness on the X beam:

![glide-motor](videos/2020-06-19_clank-glide-motors.mp4)

![wiring](images/2020-06-19_clank-xy-wiring.jpg)

Here's all of my notes (for the record) from this run together:

- current setup doesn't actually make 300mm sq motion, 
    - downgrade machine for 200mm ? for students ? idk, cuteness over everything haha ... 
    - really, 250x250x250 (for 260 actual) is *oodles* at this scale
    - irregardless, making these assys scalable should be no problem 
    - I'm sketching out now at 256, for the bytes, man, and probably just 200mm in y of actual bed space, that's *enough* (the 56 for tool hangover) 
- please don't make me take these brims off again 
- same with the non-chamfered M4 hex supports 
- the xy frame is really not stable enough in terms of parallelism on its own, neds the front brace from the z-assembly, or separately if standalone 
    - perhaps it's worth extending the front of the design to put a brace directly in front. you'll want the tool changer base here anyways, right? 
- yl and yr pulley 'cap's are slightly different lengths, confusing when assy 
- same re: motor mounts, label... 
- entry chamfers on printer-face hexes would do well 
- M4 on motor pulleys YL/YR are not thru size, should be 
    - this is also true of thru-holes all over the place, pls review 
- belt gap on YL/YR not large enough (fixed in CAD)
- the long x-carriage thru bolts (thru the pulley idlers, etc) are tough to get together, try loosening the hex in the upper layers to ease threading, and open the thru hole up as well. 
- is the move to order all new extrusions and go again, for z, and keep this XY stage, or is the move to order some extra-sized z-extrusions? this doesn't match / I'm sure it'll squish enough. Maybe just go again from the top, yeah? 
- same note on the thru-holes for the M5 on tapped ends of extrusions, should be clearly 'thru' - not snug up. a chamfer along these edges would also be bonus. The real constraint is in the flathead taper, not the walls here. 
- and the chamfer, 100, and the top edge can be chamfered all the way down to avoid support 
- watch screw lengths for beam-end-taps, these M5x10s feel like they could be M5x15s, and I think some CAD is off too. 
- the cable 'peg' in the rear of the x carriage should be - just one - and maybe 10mm south of where the top one is currently: the lower peg currently interferes with the rear frame element, and the top currently interferes with the pulley / motor shaft 
- the cable 'pegs' on the rear of the yl / yr carriages are not useful and instead just cause awkward near-collisions / rubs with the rear frame. delete them 
- framing: consider along with the base / z, which brackets, and where? pls include in fusion assy, to sanity check clearances 

## 2020 06 18 

It's been mostly a code party this week, getting OSAP up to bus motion to this thing, but when my noodle is cooked at EOD I've been assembling this... 

![assy](images/2020-06-18_clank-xy-fab.jpg)

## 2020 06 13 

Just finishing up with prints etc, I did finally try out this 'print modifier' thing in Slic3r. Here, enforcing solid infill around a pinch point I'm not particularely structurally stoked about. 

![pm](images/2020-06-13_print-modify-01.png)
![pm](images/2020-06-13_print-modify-02.png)

On completing the XY stage, and looking at total printing time, it's 45h total to do the XY alone (including 3h for motor plates / but that's rounding). This is at 0.2mm layer heights. My initial estimate for the whole machine was 60h, so I'll likely overshoot that by ~ 20h for all of the bed / frame / etc. Going forward, definitely something to be cognizant of. I figure I *will* ask for ~ 3 more Prusas for the basement. 

At this point, it's worth considering the future. Besides the machine itself, there's a lot more to get to.
- a print bed (pcb heater / PEI coating) 
- a print head (pcb heater / stepper board) 
- a milling bed / milling head (this is easier) 
- touch probe / load cell probe (this last) 
- what is the hand-tool kit that students receive? 
- a g-code interpreter for first-runs / expanding to codes for other processes 
- busses define a new module board & phy
- PSU & central node mounting 
- is central node USB connection, RPI, ESP32? 
- completing MTM and instructions (this is big!)

My expectation is that software can come last, and if I do a good enough job making frameworks work well, I can get help with this most-easily. For tool heads, I might skip the development of an automatic toolchanger in favour of otherwise robust tools - it's easy to splice a changer in between the machine and the head later on. Or, the middle ground is a screw-to-fix kinematic setup. I should probably just plan on that.

Circuits are now the longest lead time item, so while I need to assemble this XY stage in order to have a good-enough controls test platform, I need to start focusing efforts in on finishing the bus / osap tools, and get things moving around confidently. That will define the PHY that I use and will let me develop that D21 mass-produced board. If I do well enough with a moduul type thing, we could even have students assemble modules-on-daughters to make steppers, heaters, etc - and the bed especially could just be a mass-ordered (albeit large) PCB with a soldered on moduul / one or two FETs to power. 

For a good system config, I expect the move is to have a Raspberry PI living on-machine, from here we can serve a local instance of osap-tool with well configured / understood serialport implementations, etc. Browsers would be remote, but could also run on-PI, though PI opengl precludes some success in ThreeJS browser implementations... oy. However, it could be possible to avoid the use of a router: one of the motor boards can have USB in and bus-out, and serve as the head.

Probably the biggest hanging question is about how buss'd drops know what they are. Since busses are basically stars, there's not a good way to know which element is which - they're all just elements on the medium. I am partially in favor of sending a DIP-switch on each moduul, so that no one has to flash any IDs.

## 2020 06 12 

I want to start printing these things, since parts are arriving, and I want the XY stage as a controls baseboard. The big tasks, to finish, are to set up cable routes and to finish out the corner brances. 

My initial thought was to run cables in the free space near the rear of the machine... but the motors / motor wiring will all be sorted out here. I think I need to arrange chains vertically. Of course the very best would be to simply take flat flex cable across either run, but it needs some support. I think the move is to print little tiny clips that grip the tape to 'close' the concave side of it. Then, pin the tape in place at ends, and it's a modular tape chain.

The YL->YR motor bus can simply extend between the two motors, easy. 

I've tried this cable holder, which grips the tape rails:

![clip](images/2020-06-12_tape-clip.png)

But of course the tape's bending causes a change of it's curvature (i.e. the part of the tape that is bent is flat, not curved) pulling the clip apart. So I need to rethink this. 

First, this tape is a bit too big. That's easy enough to fix, I'll use the smaller lever-lock I have here. Then there's the clips. I guess in the end, I need to wrap the whole tape up. Really the best way to do this is with the good old sleeve and heat shrink, but that's also going to be a PITA for novices doing this. Given the thing's size, the sort of obvious other move is to use some 'medium-hard' tubing with an sleeve wrap. Perhaps I can use those long M4's as posts, find a tube with ahn ~ M4 ID, and then kind of hose-clamp things together. This feels all together more sound. So,

Then I'll route one to YL, YR via YL and across, and one to the X Carriage, via the post / corners. Or, better, I can make a generalized 'clip' for the rear extrusion, and just use two of those, separate from the post / corners. 

For hosing: 5/32" is 3.96mm, so an appropriately soft hose here would be ideal, but might never come off. 

OK, and now I'm through the posts as well, which, like the X-Beam to YL/YR, is so so easy when we just tap the extrusion ends. This only adds $3.50 per side, so it seems very worth it to me, again. I think the bottom / chassis etc will *not* need these, but again we'll see... some additional tens of dollars is IMO better than the struggle to assemble otherwise. 

So, last order before printing these things is to set up to mount things to the face plate. I do expect that 'real' future machines will integrate the tool changer with this plate, but I'll give myself some handles for quick'n dirty tools to demo. 

OK, done w/ that, exported the assy, should do a decent check & see, then set up the print train. 

![all-together-there](images/2020-06-12_clank-xy-allup.png)

Also - have to place that McMaster Order. To McMaster the ball bearings or not...

## 2020 06 06 

I've extended my stay in Canada up to Tuesday, and I want to use that as an excuse to finish detailing out the XY stage on Clank, since I've my head all loaded up with it currently. Optimistically, I can get through that today. 

I'm *almost* tempted to rebuild the YL model. It's a real humunculus. At this point I think, it's fine, I will build the YR model and then see about squeezing a belt fixture in to the YL as well, if I want to take it on I will. In doing I could probably eek out another ~ 15mm of saved length... which is appealing, but, you know, time. 

The front roller is the appropriate choice if I assume there will be a big weight on the EE, otherwise I should really tuck the roller in behind the x beam... I'm supporting the weight of the y motor back here, and allowing it to dip might see the carriage interfere with the belt. 

I can line this up nicely with the X-constraining bearing set on the opposite side. 

It occurs to me also that the width / length of the x-beam and the structure are guaranteed by misumi extrusions-cut-to-length to be +/- 0.5mm each, meaning that the y-axis carriages need to accept up to 1mm complete of lost length. Luckily we have kinematic design, so the floating side should handle this easily, but that it - *just* the floating side - needs to have enough leniency to handle this. Beware! Offset there cannot be simply 0.75mm, but should be ~ 2mm or so. 

OK, I got through the majority of this today, save for the corner caps. I'm at least going to put it down for now, until my Canadian tenure is up. 

![xy-assy](images/2020-06-06_xy-assy.png)

You're ready to fabricate the XY stage, more or less,
- watch small details in YL and YR: rear offsets are off by 0.25mm, not sure how. also, actual y-depth in the assy is off from the model, where did you measure ? check check 
- belt is similar: teeth on YL and YR not perfectly aligned, why?
- need to design chassis corners for this stage 
- this maybe means getting in some corner brackets, to see how much 'stick-out' is necessary, and to feel up how things go together 
- align on bottoms: xcarr, ylcarr, yrcarr 
- rename for elegance, name bodies xcarr-top-plate etc 
- check that belt planes are aligned, for grace 
- I know you don't want this *right now* but recall that you will be wiring these things up. be kind to future-self who is doing this, and setup for wiring looms now. it's an integration station. 

## 2020 06 05 

Another tricky note that I forgot about: the motor plate should really be phenolic, even in the kit machines we do. Of course, option for printed, but phenolic will mean lots more current to pull before things to soft. So, a plate. 

In the end, this resolves pretty well. Final detail is just some awkwardness around M4 length on the short ones... sorted that w/ an additional washer at the bearing head. Then there's this awkward sideways-screw, for which I currently have an entire hole cut out, but I think I can do better... here's now, which has a length problem:

![badboi](images/2020-06-05_bad-hole.png)

Now I can make this more of a tray... though in order to fit that BHCS I properly need a bit thicker plates. These are ~ 8mm thick now, for 5/16" phenolic sheets if it ever comes to that, but the next tick up is to 9.5mm, for 3/8" sheets. Things start to get kind of hunky here. Oh the indecision! I think I'll stay beefy: want to mill things with this, yeah. The motor plate will be phenolic, 5mm though. 

Alright, upped that thickness to 9.5 for some beefy friends, and now the hole is a 'tray' that I can print upside down and should make assembly friendlier... 

![tray](images/2020-06-05_tray-hole.png)

Here it is relative the rhino sketch:

![compare](images/2020-06-05_compare.png)

In Rhino, I had thought that I could sneak the idler bearings in sort of 'under the motor mounts, which may not be true. That will increase the size of the motor bubble on the y-carriages if I detail this in the same way, so I should be careful about that, it could add a good amount of width to the machine overall. Probably in Y I end up moving the idlers to be adjacent the motor, and maybe the motor mounts in at 45' or some such, to get the screws out of the way. I suppose that's next. 

Getting these things 'under' feels like tough work... mostly because it means abandoning the hot-plate Phenolic at the motor interface. I could put the motor on *top* of the extrusion... yeah, roughly like:

![ytop](images/2020-06-05_y-top.png) 

This might even mean I save some space down the road. Bless bless. The caveat (dangit) is that the motor mounting screws are somewhat inaccessible here. 

Side note: I can work belts through right 'past' the motor here, that'll be great for belt setup on the X axis. For the y mounts, the move might be to fix the motor on its plate, then set up a rotation on that plate to tension. 

Yep, alright... I'll rotate the motor plate, and get a tiny platform off of the top plate to secure against the idlers, to keep them stiff. Then I have one more of these semi-awkward 90' connections between the interior plate and the top plate. And I should / can integrate the top plate w/ the belt hold-down elements for the x carriage. 

It's getting pinchy in here! Honestly, gracefulness be damned I'm going to ask for tapped ends on these things and just hammer them home. No shame. 

Gotem... 

![detail](images/2020-06-05_y-top-detail.png)

These mesh well, 

![detail](images/2020-06-05_y-top-together.png)

That should take care of the nasty bits, so here's the current list for XY completion:
 - x-belt needs to meet the y-device, you should be able to sneak it in between the extrusion-back and the motor-plate pivot, also ideally the x-belt comes down in height somewhat... in a truly beautiful world it's on-plane with the y motors. go for this. 
 - the y-right side needs a d-sign, try a new model but similar ideas as this, it nicely just wants that rotary constraint... might desire something new from you, will see 
 - the corner caps would be last order then, before you're set to try building one of these / ordering extrusions etc. these four parts should aid in length-setting for chassis assembly, and also hold your belts down for the y axis.

## 2020 06 04 

Stole an hour or two tonight to look at detailing for the X gantry... 

![x-det](images/2020-06-04_x-detail.png)

This is decent, sturdy union between the face of the thing and other plates. Will get a bit hairier where the motor arrives on the rear face... currently these prints orient really well, the rear will be a bit more of a 'stack' I think. Realizing also that really I have done this before, on the Ratchet, so that was a nice warm-up for printed-with-extrusions machine type. 

OK, around the back... this awkward duckling, which should pin through the idlers, through the motor plate, to the top plate. I want also to tie that down to the bottom plate, so I need some decent way of maneuvering another stack below it, with thru-screws, etc... maybe I'll get that out tomorrow AM. 

![x-duck](images/2020-06-04_x-duck.png)

## 2020 06 03 

Pending extrusions, I'm a little bit back at the drawing board here. A rough plan is to do over the ratchet design, one 'size' down, going 608 -> 624 bearings, and N17 Reduction -> N17 Direct. 

I'd love also if these carriages could incorporate sheets of phenolic, with minimal printed parts. This would also greatly reduce fabrication time, and increase stiffnes. 

Probably the biggest choice is whether / not to move the motors about on the chassis, or to use pulleys. I like pinions for the 2x stiffness increase and the mass will probably reduce chatter during milling. It makes the carriage a wee bit more complex, but makes endpoints wicked simple and reduces overall complexity by some fold. So that's probably the move, then I just need to sort out the motor mounting position. 

Honest to god the simplest way to do this might be just the absolutely straightforward pinion-on-face 'std issue' axis, three up. Pin the motor on the 'top' plate of the carriage... 

OK, I ran some variants of axis designs in Rhino:

![variants](images/2020-06-03_variants.png)

And I think I like this the most, for balance on shrinking & rigidity:

![ds](images/2020-06-03_downselect.png)

This has a nice face on the opposite side to the motor, that should make an excellent ee-bolting-surface. I also like having the motor in the rear, bundles of wires etc all 'behind'.

![ds-face](images/2020-06-03_downselect-face.png)

Finally, I get the sense I will have an OK time tucking this thing in amongst two y-axis of similar design:

![yt](images/2020-06-03_y-tuck.png)

This measures 100mm from tool-center to outside of y, and I should be able to win back ~ 15mm on the end that isn't constraining xy motion. Design of the Y axis will be coupled: one side will pin all DOF, the other side will just be the anti-rotation, so two rollers only on the... right. 

But, to start, CAD on the x axis is the move.

Big open question still is Z axis work. Perhaps worth roughing something out there before getting on to detail on the x. First order to detail the Z might actually be to flesh out the Y somewhat, just to have a sense of where ends are, relative the top-z (lowest value, highest bed) position is, if I can hang motors anywhere in that space. 

So, if I tuck these in like-so, I can do even better. This layout does 300x300mm build in 465x430mm dimensions:

![yt](images/2020-06-03_y-tuck-under.png)

![yt](images/2020-06-03_y-tuck-under-xy.png)

So I should rough out a z-solution. I'm not averse to using three motors, let's see... 

Yeah, might be dead simple: three motors, careful XY constraint on one rail, rotary on the second, third is just floating z-constraint none else. So the design exercise there is for elegant / simple constraints & pulleys to match, to pinch inside of all of these things. 

Last detail would be to find out about t-slot fittings... and run #s on those, for price. 

In a sense, the bed can really make or break this thing. First, I need some hang-over space in the front, if I'm going to hoist tools that have some z-depth, and still have others that have zero and can make the bed. The kind of front-brace is tough, 

This works, save for the flipped-about y motors interfering:

![yt](images/2020-06-03_z-but-not-y.png)

So, since I want to bring the face-plate of the x carriage further towards the face of the machine, I can un-flip those y motors and push the x-beam junction forwards... This makes a much *deeper* machine, 500mm long where it was ~ 450 before. It does nicely resolve wiring such that all motor drivers would be facing *up* which also makes this newly well-suited for PNP and easy-breezy-pen-plotters etc... But it does make for one awkwardly floppy unfixed-side of the y:

![yt](images/2020-06-03_z-floppy-y.png)

This is non-terminal, but not elegant either. 

I do like that this makes a nice, simple, maximal *frame* for a bed, meaning that fixing in modular beds should be easily made graceful. 

Finally, I could do this by instead adding what looks like ~ 15mm to either end of the x carriage, for 30mm in bonus width, getting back that 30mm of length. As a bonus feature here I get a wider tool plate, by 30mm, which is worth more to be in the x carriage than it is in the y carriages. 

So yeah, this is *just* a touch less floor area (really, identical) but has + 30mm on the tool face, which is much preferrable to + 30mm in depth. 

![yt](images/2020-06-03_z-bed-frames.png)

The *right* option in that photo is the one. I still have to figure how I'll support these ends... The cross bar in the front makes this more difficult, I can't totally wrap one column to do XY constraint as I had planned. I could move that bar north, wrapping above the tool bodies, but that defeats the idea of having relatively clear space up there for whatever-the-hell I end up doing with holding on to unused tool bodies, and especially would make it tough to deliver wiring looms to tools. 

I could, I suppose, really mess things about and put xy/r constraints on the rear two posts, then pin height on three unrelated points at front left, front right, and rear middle. This might really ruin some noodles... but is a good lesson in what is really going on in kinematic design. I *dont* like in any case that all of the load is going to be going through one bearing per axis here... but that's besides. 

![yt](images/2020-06-03_z-sep.png)

Yeah, so I'll do it like that: rear is one XY constraint, one R constraint, and three pulleys: two front left and one rear center. Bed is a big floater, with a T down the middle, expecting to mount fixtures inside that frame. This'll be 6 motors in an assembly! 

For assembly of this thing, might not be super fool proof. The critical lengths have to do with the insets on the feet etc. Really, success here will be about doing documentation well. Per tools, it might make sense to go for auto-tool-changing *later* and maybe propose it as a project during MTM week. Or whatever, decide later. I also worry about the assembly order: ideally axis would slide-on slide-off, but I think they're going to have to kind of pinch together all simultaneously. This coupled with any amount of sensitive frame assembly could be a PITA. It'd be really great at least if the lower frame & z-system could assemble separately from the XY stage, perhaps it 'drops on' to the z-legs. This would mainly mean finangling the pulley and pulley idlers onto the z-posts... Seems like there's a reasonable route to this.

![yt](images/2020-06-03_z-sep-top.png)

That about closes this design phase out. I'll save here, and go on to detail on the x carriage next. Overall I like the direction, but as always success will be in the details. Particularely, I have familiarity to gain on the extrusion systems: braket types and costs etc. But I'll bet this will be fun, overall. 

## 2020 06 02 

Curious about clank-via-extrusion... so I'm mocking that up this morning. My biggest uncertainty is w/r/t how to make the bed. If I do use extrusions, I'd like for everything else to be 3D Printed. 

The move here *feels* more like the bed-lifting-by-edges than the bed-cantilever. And dang, maybe it's even 3x motors to lift it, as well. 

![extrude](images/2020-06-02_clank-extrusion.png)

In total assembly and fabrication time, I think almost certainly the sheet-panel fab will be less: wripping Phenolic on the zund is much faster material-per-minute than even our farm of Prusas. We would have to do big portions of the assembly... 

The beam is three shots of layup, as currently designed.

![steps](images/2020-06-02_clank-beamsteps.png)

The bed might be one shot, and the chassis is at least two, with the last likely being a bit of an ordeal, pinning sides together as well as the bed in-place. I should remember that in this scheme I am aiming at an expoxied-up x-carriage as well. 

![steps](images/2020-06-02_clank-chassissteps.png)

To discuss, I think that the chassis / phenolic machine could probably be a better machine overall: lighter, somewhat stiffer, and offers the elegance of combining panel covering w/ chassis strength. Because of layup, potentially straighter. However, it feels as though it actually ends up more expensive in terms of chassis material cost: 36x48" sheet of 3/16" phenolic is $200 usd, and might not even be enough, while a set of extrusions for the similar sized machine are $90. Both will use a similar count of bearings, etc. The phenolic has the added benefit of not mounting motors into plastics that soften with temperature. 

Spiritually, the phenolic machine is like the milled-circuit board equivalent to the extrusions' breadboard. Slightly better, but more effort. Harder to modify if you mess up (this is why *we* would do the glueing). I also can't imagine making the phenolic machine without those reference flats, which are not available across the board. I also get a sense that the extrusion machine is friendlier to smaller sizes, and that the fab-class machine would realistically be ~ 200x200 bed size. It would additionally be easier to swap sizes: just go bigger extrusions. There's certainly more assembly required, and there's the open question on how to deal with the bed. 

Finally, the extrusion potentially scales exponentially, to anyone with a printer. That might be the last ticket. It also feels like, if we ship these to students, they would have a stronger sense of how to make new things: just axis, bolted to axis, etc. Basically for that reason alone, and for the ease of remote asssembly / etc, extrusion is probably it. 

As is tradition, detail design should proceed from the head out. I would even be tempted to use the same kind of pinion-riding gantry for *everything* ... controls are what will make this thing sing. 

## 2020 05 22

Half hour today... here's a roller component I can pin onto sheet stocks...

![roller](images/2020-05-22_std-roller.png)

Maybe more like

![roller](images/2020-05-22_std-roller-l.png)

## 2020 05 15

### PIP is now Clank

New work here kickstarted with the idea that we might try to build machines (more likely kits) for fab-class students.

Also, was daydreaming about 'clank' in the line at the grocery store. Thoughts are to make the z-axis just a big ol' gantry box-drop thing, on a single leadscrew / ballscrew. Or a reduced belt, although, I would have small confidence in a belt drive handling that z-axis, less it were the GT3 belt / reduction move, which is maybe the trick. Y- and X-axis would be dead simple GT2 drives, with the Y potentially being one continuous belt to grip both sides of a beam, consider the old idea of, instead of making belt routing complex, just grabbing a take-off of the belt on the 'other' side of it. Arrange it so that the motor drivers are all sticking tail up, Y and Z at the back of the machine (leadscrew thoughts), and X on the gantry. Gantry fully kinematic, one side torsion and x-, the last just rotation around the important rail. Chassis is Garolite, epoxied, or alternately acrylic, with printed inserts where flexures / major mechanical mounts etc. Lots of flushness...

### Design First Steps

My biggest two questions are about the Y and Z drives. The Y w/ single belt is appealing but I don't like the idea of compound belt tensioning. I should get a measurement for standard GT2 belt stiffness in mm/N/mm (mm of deflection per newton, per mm of length). That should just be a spring constant... and should be available. For simplicity, it might not be that bad to run two Y motors, save the slight complexity in control and cost of motor and board, etc. But it does afford a nice symmetry to newtons-force available and work to be done. *and* makes me feel better about going forward to wider / faster friends, like a PNP chassis. OK.

The z - I am satisified with the reduced GT3 drive I have been using on larger axis. It's the right amount of oomf, and I can work the motor mount into the rear of the z-spine. I think I might just go ahead and doodle this tonight in fusion, or Rhino. The basic move is to overload the back of the machine with 'services' to keep the front end about as wide as the bed, plus minor overhangs. Elegance be damned, I'll use two Y motors, busses be our saviour.

Since the y- and x- motors are just direct pinion to belt, both can be tensioned on motor rotations. Motors should be mounted into phenloic, not printed inserts. The design challenge is about lining up phenolic sheets.

OK, the first trick is in the x gantry. The sheet that mounts the motor is the 'auxilliary' in the beam, also holds the bearings that pinch to the x-constraint.

Not sure how to CAD this thing together. What I want is:
 - basic outlines, alignments of sheets, knowledge of locations of idlers, motors, limits, etc.
 - a core budget & cost estimate
 - a plan to assemble the cage.

I still think Rhino is the tool.

Well, this is still a challenge but I think the basic idea is sane. I should really be focused on software at this point, but it'll be fun to have a sketch of this for that meeting (22nd). As is normally the case proper success here will rely on some cleverness in the layout... one big pain at the moment is the y motors placement... they really want to get tucked in under the x beam, and this will call for the plate that the y-motors are mounted on to be offset from the machine endplates (left and right chassis plates). I am hesitant to flip them upside down as I'd really like the controllers to be visible easily, to debug lights and hit the reset... In summary, while I had previously thought the x-beam would be simple, it might make sense to make some allowances for these things in that element.

Otherwise, it's really all just little details. At this stage, we it probably makes the most sense to just clean up this sketch to 'finish' it, get a rough idea of sizes, cost, and post that. RIP. Keep writing. Move these notes to the clank log.

OK, long walk. Couple of resolutions:
 - as often as possible, idlers should mount directly into phenloic. With an M4, and a hex nut on the other side, and *two* M4 washers (just *one* for rollres) as a bushing/standoff, and a 3.9mm hole in the profile, I feel good about this as load bearing and cantilever-ing. That will simplify a *lot* of design elements, and mean many less 3D Printed (slow) parts, in favor of more zund (fast, strong) parts.
 - Idlers as well as Rollers can be one 625zz bearing.  
 - Preloaded elements should be printed, still, but should mount flush into zund-ed parts. Care will have to be taken to make sure that these don't rotate / otherwise track out of position.
 - Might be that the X Carriage should be phenolic as well. Those last steps are always a bit soft, it would be awesome to cinch it up.
 - I would like to second the above point. This is easy to do and the wins are obvious.

My last questions are about the structural elements in the chassis, and their relation to the bed. Should recall that the load travels ultimately through the edges of the chassis through to that spine. The rear is easy to work out. The front basically needs some braces. This is made more difficult by the fact that the gantry needs to slide off the front. The brace is really necessary, so I'm going to say this plate is fastened on, not glued. Nah that's trash.

This is fine, I'm leaving it for now. The bummer about the brace is that it seems likely to make loading sheet stock a pain.

Well, not sure. Right now it's 300x300, perhaps just wants to be 200x200. That'll make it cute, but will feel a lot bulkier around the edges. I still detest machines where the bed moves in the y plane, can't give you a good reason. Somehow this one still feels like it's missing the elegance. I should be easier on myself though, it's just around 450mm on either side, and about 500mm tall. With 100mm less in either dimension, 350mm is about the width of this laptop.

If we remember we were going to do this as a kit... Maybe there is a simpler way still? My sense is that this might be close, but it could / should still be simpler - or perhaps just smaller. Easy to win another 20mm by taking 10mm of width off of the toolchanger, but I'm fond of a 90mm width there.

![clank-dwg](images/2020-05-15_clank-dwg.jpg)
![clank-cad](images/2020-05-15_clank-sketch.png)

