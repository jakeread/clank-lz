# Clank-LZ

Clank-LZ is a variant of the [Clank](https://gitlab.cba.mit.edu/jakeread/clank) machine platform developed as a machine kit for [How to Make Almost Anything 2020](https://fab.cba.mit.edu/classes/863.20/), which was taught remotely at MIT in the fall term. 

The [dev log is here](log/clank-log.md) and I've written some [reflections on clank-lz as a class kit here](log/clank-lz-reflections.md), if you're interested in my experience and collected notes from other TAs and students in the class. 

## What it Does

Clank-LZ is a circuit mill, meaning that it cuts traces from copper-faced phenolic stock, i.e. FR1 stock. It's just a small, precise CNC mill. 

## How to Build Clank-LZ

There's a full [build guide for clank-lz here](build/README.md) including a [BOM](https://docs.google.com/spreadsheets/d/1HzgdPIvii0NI3s8gkg9xFz3LI2AqHLVXkGTv0clRpXw/edit?usp=sharing) [pdf](build/clank-lz-bom.pdf) [xlsx](build/clank-lz-bom.xlsx]) [ods](build/clank-lz-bom.ods) and assembly videos, and the most recent CAD files are [f3z](/cad/2020-09-29_clank-lz.f3z), [step](/cad/2020-09-29_clank-lz.step), and [.3dm](../cad/2020-09-29_clank-lz.3dm). The hardware costs around $400 (including motors) and controllers can cost anywhere between $250 and $100. 

To build one, you'll need a 3D Printer (any will do), allen keys, scissors, and not much else. An arbor press helps for the spindle assembly. 

## Running Clank-LZ

The machine was developed alongside a [standlone controller documented here](https://gitlab.cba.mit.edu/jakeread/clank-lz-controller), but is equally well suited to any off the shelf GCode interpreter. We like [tinyg](https://github.com/synthetos/TinyG/wiki) but any will do. 

## Images

![clank](log/images/2020-07-30_clank-lz-fab-01.jpg)
![clank](log/images/2020-07-30_clank-lz-fab-02.jpg)

![milling](log/videos/2020-08-19_clank-mill-system.mp4)
![sys](log/videos/2020-08-19_clank-mill-beauty.mp4)

![img](log/images/2020-08-19_clank-mill-02.jpg)
![img](log/images/2020-08-19_clank-mill-03.jpg)

## Clank-LZ as a Milling Machine

The machine does just-fine as a general purpose CNC mill as well, capable of milling wood, machinist's wax, and engineering plastics like Delrin and HDPE. 

![mill-hdpe](log/videos/2021-01-05_clank-mills-hdpe.mp4).